package edu.ncsu.csc216.pack_scheduler.course;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test Class for ConflictException Class
 *
 * @author Abhay
 */
public class ConflictExceptionTest {

	/**
	 * Test method for ConflictExceptionString.
	 */
	@Test
	public void testConflictExceptionString() {
	    ConflictException x = new ConflictException("Custom exception message");
	    assertEquals("Custom exception message", x.getMessage());
	}

	/**
	 * Test method for ConflictException.
	 */
	@Test
	public void testConflictException() {
        ConflictException y = new ConflictException();
		assertEquals("Schedule conflict.", y.getMessage());
	}
}
