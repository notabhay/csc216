package edu.ncsu.csc216.pack_scheduler.course;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests checkConflict method from Activity
 *
 * @author Abhay
 */
public class ActivityTest {
    /**
	 * Test method for CheckConflict.
	 */
	@Test
    public void testCheckConflict() {
		Activity a1 = new Course("CSC216", "Programming Concepts - Java", "001", 4, "sesmith5", "MW", 1330, 1445);
        Activity a2 = new Course("CSC216", "Programming Concepts - Java", "001", 4, "sesmith5", "TH", 1330, 1445);
		try {
			a1.checkConflict(a2);
			assertEquals("Incorrect meeting string for this Activity.", "MW 1:30PM-2:45PM", a1.getMeetingString());
			assertEquals("Incorrect meeting string for possibleConflictingActivity.", "TH 1:30PM-2:45PM", a2.getMeetingString());
		} catch (ConflictException e) {
			fail("A ConflictException was thrown when two Activities at the same time on completely distinct days were compared.");
		}
		try {
			a2.checkConflict(a1);
			assertEquals("Incorrect meeting string for this Activity.", "MW 1:30PM-2:45PM", a1.getMeetingString());
			assertEquals("Incorrect meeting string for possibleConflictingActivity.", "TH 1:30PM-2:45PM", a2.getMeetingString());
		} catch (ConflictException e) {
			fail("A ConflictException was thrown when two Activities at the same time on completely distinct days were compared.");
		}

		a1.setMeetingDays("TH");
		a1.setActivityTime(1445, 1530);
		try {
			a1.checkConflict(a2);
			fail();
		} catch (ConflictException e) {
			assertEquals("TH 2:45PM-3:30PM", a1.getMeetingString());
			assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
		}
        try {
			a2.checkConflict(a1);
			fail();
		} catch (ConflictException e) {
			assertEquals("TH 2:45PM-3:30PM", a1.getMeetingString());
			assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
		}
        try {
            a1.setMeetingDays("A");
            a2.setMeetingDays("A");
            a1.checkConflict(a2);
        } catch(ConflictException e) {
            fail("A ConflictException was thrown when two Activities have meeting days as A");
        }

        a1.setMeetingDays("TH");
        a2.setMeetingDays("TH");
        a1.setActivityTime(1234, 1235);
        a2.setActivityTime(1236, 1237);
        try {
            a1.checkConflict(a2);
        } catch(ConflictException e) {
            assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
        }

        a1.setMeetingDays("TH");
        a2.setMeetingDays("TH");
        a1.setActivityTime(1236, 1237);
        a2.setActivityTime(1234, 1235);
        try {
            a1.checkConflict(a2);
        } catch(ConflictException e) {
            assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
        }
	}

    /**
     * Tests SetActivityTime
     */
    @Test
    public void testsetActivityTime() {
        Activity a1 = new Course("CSC216", "Programming Concepts - Java", "001", 4, "sesmith5", "MW", 1330, 1445);
        try {
            a1.setActivityTime(-1000, -1000);
        } catch(IllegalArgumentException e) {
            assertEquals(null, e.getMessage());
        }
        try {
            a1.setActivityTime(1234, -1000);
        } catch(IllegalArgumentException e) {
            assertEquals(null, e.getMessage());
        }
        try {
            a1.setMeetingDays("A");
            a1.setActivityTime(0, 1235);
        } catch(IllegalArgumentException e) {
            assertEquals(null, e.getMessage());
        }
    }
}
