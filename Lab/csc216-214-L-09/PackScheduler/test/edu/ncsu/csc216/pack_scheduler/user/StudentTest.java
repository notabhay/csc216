package edu.ncsu.csc216.pack_scheduler.user;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests Methods in Student Class
 *
 * Note that test methods for all getters have been omitted. They will be tested
 * through other methods.
 *
 * @author Pradhan Chetan
 * @author Alexander Depolt
 * @author Abhay Sriwastawa
 */
public class StudentTest {

	/** Test first name */
	private static final String FIRST_NAME = "Stu";
	/** Test last name */
	private static final String LAST_NAME = "Dent";
	/** Test id */
	private static final String ID = "sdent";
	/** Test email */
	private static final String EMAIL = "sdent@ncsu.edu";
	/** Test password */
	private static final String PASSWORD = "pw";
	/** Test max credits */
	private static final int MAX_CREDITS = 15;

	/**
	 * Tests that hashCode works correctly.
	 */
	@Test
	public void testHashCode() {
		new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD);

		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s2 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s3 = new Student("wong", LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s4 = new Student(FIRST_NAME, "Wert", ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s5 = new Student(FIRST_NAME, LAST_NAME, "pchetan", EMAIL, PASSWORD, MAX_CREDITS);
		Student s6 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, "p@ssw0rd", MAX_CREDITS);
		Student s7 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, 14);

		// Test for the same hash code for the same values
		assertEquals(s1.hashCode(), s1.hashCode());
		assertEquals(s1.hashCode(), s2.hashCode());
		assertEquals(s2.hashCode(), s1.hashCode());

		// Test for each of the fields
		assertNotEquals(s1.hashCode(), s3.hashCode());
		assertNotEquals(s1.hashCode(), s4.hashCode());
		assertNotEquals(s1.hashCode(), s5.hashCode());
		assertNotEquals(s1.hashCode(), s6.hashCode());
		assertNotEquals(s1.hashCode(), s7.hashCode());
	}

	/**
	 * Tests setFirstName().
	 */
	@Test
	public void testSetFirstName() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);

		// Testing for null first name
		try {
			s1.setFirstName(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid first name", e.getMessage());
		}

		// Testing for empty string first name
		try {
			s1.setFirstName("");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid first name", e.getMessage());
		}
	}

	/**
	 * Tests setLastName().
	 */
	@Test
	public void testSetLastName() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);

		// Testing for null first name string
		try {
			s1.setLastName(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid last name", e.getMessage());
		}

		// Testing for empty last name string
		try {
			s1.setLastName("");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid last name", e.getMessage());
		}
	}

	/**
	 * Tests setId().
	 */
	@Test
	public void testSetId() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);

		// Testing for null ID
		try {
			s1.setId(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid id", e.getMessage());
		}

		// Testing for empty ID
		try {
			s1.setId("");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid id", e.getMessage());
		}
	}

	/**
	 * Tests setEmail().
	 */
	@Test
	public void testSetEmail() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);

		// Testing for null email
		try {
			s1.setEmail(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid email", e.getMessage());
		}

		// Testing for empty email string
		try {
			s1.setEmail("");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid email", e.getMessage());
		}

		// Testing for invalid email that does not contain '@'
		try {
			s1.setEmail("a");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid email", e.getMessage());
		}

		// Testing for invalid email that contains '@' after full stop
		try {
			s1.setEmail("a.a@");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid email", e.getMessage());
		}

		// Testing for invalid email that does not contain '.'
		try {
			s1.setEmail("a@");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid email", e.getMessage());
		}
	}

	/**
	 * Tests setPassword()
	 */
	@Test
	public void testSetPassword() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);

		// Testing for null password
		try {
			s1.setPassword(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid password", e.getMessage());
		}

		// Testing for empty password field
		try {
			s1.setPassword("");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid password", e.getMessage());
		}
	}

	/**
	 * Tests setMaxCredits()
	 */
	@Test
	public void testSetMaxCredits() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		assertEquals(FIRST_NAME, s1.getFirstName());
		assertEquals(LAST_NAME, s1.getLastName());
		assertEquals(ID, s1.getId());
		assertEquals(EMAIL, s1.getEmail());
		assertEquals(PASSWORD, s1.getPassword());
		assertEquals(MAX_CREDITS, s1.getMaxCredits());

		// Testing invalid number of credits that is lower than the minimum requirement
		try {
			s1.setMaxCredits(2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(FIRST_NAME, s1.getFirstName());
			assertEquals(LAST_NAME, s1.getLastName());
			assertEquals(ID, s1.getId());
			assertEquals(EMAIL, s1.getEmail());
			assertEquals(PASSWORD, s1.getPassword());
			assertEquals(MAX_CREDITS, s1.getMaxCredits());
		}

		// Testing invalid number of credits that is greater than the maximum
		try {
			s1.setMaxCredits(19);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(FIRST_NAME, s1.getFirstName());
			assertEquals(LAST_NAME, s1.getLastName());
			assertEquals(ID, s1.getId());
			assertEquals(EMAIL, s1.getEmail());
			assertEquals(PASSWORD, s1.getPassword());
			assertEquals(MAX_CREDITS, s1.getMaxCredits());
		}
	}

	/**
	 * Tests that the equals method works for all Student fields.
	 */
	@Test
	public void testEqualsObject() {
		Student s1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s2 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s3 = new Student("wrong", LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s4 = new Student(FIRST_NAME, "wong", ID, EMAIL, PASSWORD, MAX_CREDITS);
		Student s5 = new Student(FIRST_NAME, LAST_NAME, "wwong", EMAIL, PASSWORD, MAX_CREDITS);
		Student s6 = new Student(FIRST_NAME, LAST_NAME, ID, "wwong@wong.wom", PASSWORD, MAX_CREDITS);
		Student s7 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, "won", MAX_CREDITS);
		Student s8 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, 14);

		// Testing for equality parse
		assertTrue(s1.equals(s1));

		// Testing for equality in both directions
		assertTrue(s1.equals(s2));
		assertTrue(s2.equals(s1));

		// Test for each of the fields
		assertFalse(s1.equals(s3));
		assertFalse(s1.equals(s4));
		assertFalse(s1.equals(s5));
		assertFalse(s1.equals(s6));
		assertFalse(s1.equals(s7));
		assertFalse(s1.equals(s8));

		// Testing equality with null
		Student s9 = null;
		assertFalse(s1.equals(s9));
	}

	/**
	 * Tests that toString returns the correct comma-separated value.
	 */
	@Test
	public void testToString() {
		Student actual = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
		String expected = "Stu,Dent,sdent,sdent@ncsu.edu,pw,15";
		assertEquals(expected, actual.toString());
	}

	/**
	 * Tests that toString returns the correct comma-separated value.
	 */
	@Test
	public void testCompareTo() {
        Student a1 = new Student(FIRST_NAME, LAST_NAME, ID, EMAIL, PASSWORD, MAX_CREDITS);
        Student a2 = null;
		try {
            a1.compareTo(a2);
        } catch(NullPointerException e) {
            assertEquals("Null element cannot be added", e.getMessage());
        }

        Student b1 = new Student(FIRST_NAME, "A", ID, EMAIL, PASSWORD, MAX_CREDITS);
        Student b2 = new Student(FIRST_NAME, "A", ID, EMAIL, PASSWORD, MAX_CREDITS);
        assertEquals(0, b1.compareTo(b2));

        Student c1 = new Student("A", "A", ID, EMAIL, PASSWORD, MAX_CREDITS);
        Student c2 = new Student("B", "A", ID, EMAIL, PASSWORD, MAX_CREDITS);
        assertEquals(-1, c1.compareTo(c2));
        assertEquals(1, c2.compareTo(c1));

        Student d1 = new Student("A", "A", "A", EMAIL, PASSWORD, MAX_CREDITS);
        Student d2 = new Student("A", "A", "B", EMAIL, PASSWORD, MAX_CREDITS);
        assertEquals(-1, d1.compareTo(d2));
        assertEquals(1, d2.compareTo(d1));
	}
}
