package edu.ncsu.csc216.pack_scheduler.io;

import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.fail;

import java.io.FileNotFoundException;

import org.junit.*;


/**
 * Tests for StudentRecordIO.
 *
 * @author Pradhan Chetan Venkataramaiah
 * @author Alexander Depolt
 * @author Abhay Sriwastawa
 */
public class StudentRecordIOTest {

	/**
	 * Tests readStudentRecords for empty and null.
	 */
    @Test
	public void testReadStudentRecords() {
        // Tests Empty Constructor for Coverage
    	new StudentRecordIO();

        // Tests NullPointerException in readRecord
        try {
            StudentRecordIO.readStudentRecords("test-files/invalid_student_records.txt");
        } catch(IllegalArgumentException | FileNotFoundException e) {
            assertEquals(null, e.getMessage());
        }

	}
}
