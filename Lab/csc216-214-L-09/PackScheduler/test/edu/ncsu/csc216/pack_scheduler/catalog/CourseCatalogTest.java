package edu.ncsu.csc216.pack_scheduler.catalog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import edu.ncsu.csc216.pack_scheduler.course.Course;

/**
 * CourseCatalogTest File tests CourseCatalog\
 *
 * @author Abhay Sriwastawa
 * @author Alexander
 * @author Pradhan
 */
public class CourseCatalogTest {

	/** Valid course records */
	private final String validTestFile = "test-files/course_records.txt";
	/** Invalid course records */
	private final String invalidTestFile = "test-files/invalid_course_records.txt";

	/** Course name */
	private static final String NAME = "HES100";
	/** Course title */
	private static final String TITLE = "Cross Training";
	/** Course section */
	private static final String SECTION = "001";
	/** Course credits */
	private static final int CREDITS = 2;
	/** Course instructor id */
	private static final String INSTRUCTOR_ID = "pdomini";
	/** Course meeting days */
	private static final String MEETING_DAYS = "TH";
	/** Course start time */
	private static final int START_TIME = 935;
	/** Course end time */
	private static final int END_TIME = 1025;

	/**
	 * Tests constructor
	 */
	@Test
	public void testConstruction() {
		CourseCatalog cc = null;

		try {
			cc = new CourseCatalog();
			assertEquals(0, cc.getCourseCatalog().length);
		} catch (IllegalArgumentException e) {
			fail("Error while creating a Constructor of the class.");
		}
	}

	/**
	 * Tests coursecatalogmethod
	 */
	@Test
	public void testNewCourseCatalog() {
		CourseCatalog cc = new CourseCatalog();

		try {
			cc.newCourseCatalog();
			assertEquals(0, cc.getCourseCatalog().length);
		} catch (IllegalArgumentException e) {
			fail("Error while creating new catalog");
		}
	}

	/**
	 * Tests readValidCourseRecords().
	 */
	@Test
	public void testLoadCoursesFromFile() {

		CourseCatalog cc = new CourseCatalog();

		// Test valid file
		try {
			cc.loadCoursesFromFile(validTestFile);
			String[][] catalog = cc.getCourseCatalog();
			assertEquals(8, catalog.length);

		} catch (IllegalArgumentException e) {
			fail("Unexpected file error reading");
		}

		// Test non-existent file
		cc.newCourseCatalog();
		try {
			cc.loadCoursesFromFile("non-existant_file.txt");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Cannot find file.", e.getMessage());
		}

		// Test invalid file
		cc.newCourseCatalog();
		try {
			cc.loadCoursesFromFile(invalidTestFile);
		} catch (IllegalArgumentException e) {
			assertEquals("Cannot find file.", e.getMessage());
		}
	}

    /**
     * Tests Adding courses to Catalog
     */
	@Test
	public void testAddCourseToCatalog() {
        CourseCatalog cc = new CourseCatalog();
		cc.newCourseCatalog();

        // Add new course
		assertTrue(cc.addCourseToCatalog(NAME, TITLE, SECTION, CREDITS, INSTRUCTOR_ID, MEETING_DAYS, START_TIME, END_TIME));

		// Attempt to add a course that already exists
		assertFalse(cc.addCourseToCatalog(NAME, TITLE, SECTION, CREDITS, INSTRUCTOR_ID, MEETING_DAYS, START_TIME, END_TIME));

		// Add new course with similar name, but different section
		assertTrue(cc.addCourseToCatalog(NAME, TITLE, "002", CREDITS, INSTRUCTOR_ID, MEETING_DAYS, START_TIME, END_TIME));

        try {
            cc.addCourseToCatalog(null, TITLE, SECTION, CREDITS, INSTRUCTOR_ID, MEETING_DAYS, START_TIME, END_TIME);
        } catch(IllegalArgumentException e) {
            assertEquals("The course cannot be added due to an error during creation.", e.getMessage());
        }
	}

    /**
     * Tests Removing course from catalog
     */
	@Test
	public void testRemoveCourseFromCatalog() {
		CourseCatalog cc = new CourseCatalog();
		cc.loadCoursesFromFile(validTestFile);

		cc.addCourseToCatalog(NAME, TITLE, SECTION, CREDITS, INSTRUCTOR_ID, MEETING_DAYS, START_TIME, END_TIME);

		// Remove a loaded course
		assertTrue(cc.removeCourseFromCatalog("CSC116", "002"));

		// Remove a user-entered course
		assertTrue(cc.removeCourseFromCatalog("HES100", "001"));

		// Remove a course that doesn't exist
		assertFalse(cc.removeCourseFromCatalog("CSC116", "002"));

        // Remove a course that doesn't exist
        assertFalse(cc.removeCourseFromCatalog("CSC616", "102"));
	}

    /**
     * Tests Getting courses from catalog
     */
	@Test
	public void testGetCourseFromCatalog() {

		CourseCatalog cc = new CourseCatalog();
		cc.loadCoursesFromFile(validTestFile);

		// Get course from catalog
		Course temp = cc.getCourseFromCatalog("CSC216", "601");
		assertEquals("Arranged", temp.getMeetingString());

		// Get course that doesn't exist
		assertNull(cc.getCourseFromCatalog("FLE400", "001"));

        try {
            cc.saveCourseCatalog("a.txt");
            cc.saveCourseCatalog("");
        } catch(IllegalArgumentException e) {
            assertEquals("The file cannot be saved.", e.getMessage());
        }
	}
}
