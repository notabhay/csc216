package edu.ncsu.csc216.collections.list;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Testing the implementation of an array list extending AbstractList that only
 * contains elements in sorted order. Additionally, no duplicate elements are
 * allowed to be stored in the list.
 *
 * @author Pradhan Chetan Venkataramaiah
 * @author Alexander Depolt
 * @author Abhay Sriwastawa
 */
public class SortedListTest {

	/**
	 * Tests that the list grows by adding new elements then comparing the list size to the expected list size.
	 */
	@Test
	public void testSortedList() {
		SortedList<String> list = new SortedList<String>();
		assertEquals(0, list.size());
		assertTrue(list.isEmpty()); // added

		assertFalse(list.contains("apple"));

		// added
		list.add("apple");
		list.add("banana");
		list.add("kiwi");
		list.add("orange");
		list.add("pineapple");
		list.add("mango");
		list.add("pear");
		list.add("grape");
		list.add("jack fruit");
		list.add("tomato");

		assertEquals(10, list.size());

		list.add("avocado");

		assertEquals(11, list.size());
		// till here

		// Test that the list grows by adding at least 11 elements
		// Remember the list's initial capacity is 10

	}

	/**
	 * Tests for the add() function. Method creates a sorted list and adds fruits and compares the actual string to the 
	 * expected string at a particular string location. Tests adding an element that is already in the list.
	 * 
	 */
	@Test
	public void testAdd() {
		SortedList<String> list = new SortedList<String>();

		list.add("banana");
		assertEquals(1, list.size());
		assertEquals("banana", list.get(0));

		// Test adding to the front, middle and back of the list
		// front
		list.add("apple");
		assertEquals(2, list.size());
		assertEquals("apple", list.get(0));

		// middle
		list.add("avocado");
		assertEquals(3, list.size());
		assertEquals("avocado", list.get(1));

		// end
		list.add("mango");
		assertEquals(4, list.size());
		assertEquals("mango", list.get(3));

		// Test adding a null element
		try {
			list.add(null);
		} catch (NullPointerException e) {
			assertEquals(null, e.getMessage());
		}

		// Test adding a duplicate element
		try {
			list.add("banana");
		} catch (IllegalArgumentException e) {
			assertEquals("Element already in list.", e.getMessage());
		}
	}

	/**
	 * Tests for the get() function, most tests focus testing the get method at boundry cases.
	 */
	@Test
	public void testGet() {
		SortedList<String> list = new SortedList<String>();

		// Since get() is used throughout the tests to check the
		// contents of the list, we don't need to test main flow functionality
		// here. Instead this test method should focus on the error
		// and boundary cases.

		// Test getting an element from an empty list
		// Find out what the message returned would be and re-check
		try {
			list.get(0);
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

		// Add some elements to the list
		list.add("pear");
		list.add("grape");
		list.add("jack fruit");
		list.add("tomato");

		// Test getting an element at an index < 0
		try {
			list.get(-1);
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

		// Test getting an element at size
		try {
			list.get(list.size());
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

	}

	/**
	 * Tests for the remove() method.
	 */
	@Test
	public void testRemove() {
		SortedList<String> list = new SortedList<String>();

		// Test removing from an empty list
		try {
			list.remove(0);
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

		// Add some elements to the list - at least 4
		list.add("pear");
		list.add("grape");
		list.add("jack fruit");
		list.add("tomato");

		// Test removing an element at an index < 0
		try {
			list.remove(-1);
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

		// Test removing an element at size
		try {
			list.remove(list.size());
		} catch (IndexOutOfBoundsException e) {
			assertEquals(null, e.getMessage());
		}

		// Test removing a middle element
		assertEquals(list.get(list.size() / 2), list.remove(list.size() / 2));

		// Test removing the last element
		assertEquals(list.get(list.size() - 1), list.remove(list.size() - 1));

		// Test removing the first element
		assertEquals(list.get(0), list.remove(0));

		// Test removing the last element
		assertEquals(list.get(list.size() - 1), list.remove(list.size() - 1));
	}

	/**
	 * Tests for the indexOf() method.
	 */
	@Test
	public void testIndexOf() {
		SortedList<String> list = new SortedList<String>();

		// Test indexOf on an empty list
		assertEquals(-1, list.indexOf("apple"));

		// Add some elements
		list.add("apple");
		list.add("grape");
		list.add("kiwi");
		list.add("tomato");

		// Test various calls to indexOf for elements in the list
		// and not in the list

		assertEquals(0, list.indexOf("apple"));
		assertEquals(2, list.indexOf("kiwi"));
		assertEquals(3, list.indexOf("tomato"));

		assertEquals(-1, list.indexOf("mango"));
		assertEquals(-1, list.indexOf("pineapple"));

		// Test checking the index of null
		try {
			list.indexOf(null);
		} catch (NullPointerException e) {
			assertEquals(null, e.getMessage());
		}
	}

	/**
	 * Tests for the clear() method.
	 */
	@Test
	public void testClear() {
		SortedList<String> list = new SortedList<String>();

		// Add some elements
		list.add("pear");
		list.add("grape");
		list.add("kiwi");

		// Clear the list
		list.clear();

		// Test that the list is empty
		assertEquals(0, list.size());
	}

	/**
	 * Tests for an empty list.
	 */
	@Test
	public void testIsEmpty() {
		SortedList<String> list = new SortedList<String>();

		// Test that the list starts empty
		assertTrue(list.isEmpty());

		// Add at least one element
		list.add("pear");
		list.add("grape");
		list.add("kiwi");

		// Check that the list is no longer empty
		assertFalse(list.isEmpty());
	}

	/**
	 * Tests for the contains() method.
	 */
	@Test
	public void testContains() {
		SortedList<String> list = new SortedList<String>();

		// Test the empty list case
		assertFalse(list.contains("apple"));

		// Add some elements
		list.add("pear");
		list.add("grape");
		list.add("kiwi");
		list.add("tomato");

		// Test some true and false cases
		assertTrue(list.contains("pear"));
		assertTrue(list.contains("grape"));
		assertFalse(list.contains("mango"));
		assertTrue(list.contains("tomato"));
		assertFalse(list.contains("pineapple"));
		assertFalse(list.contains("apple"));
	}

	/**
	 * Tests for the equals() method works correctly.
	 */
	@Test
	public void testEquals() {
		SortedList<String> list1 = new SortedList<String>();
		SortedList<String> list2 = new SortedList<String>();
		SortedList<String> list3 = new SortedList<String>();

		SortedList<String> list4 = null;

		// Make two lists the same and one list different
		list1.add("pear");
		list1.add("grape");
		list1.add("kiwi");
		list1.add("tomato");

		list2.add("pear");
		list2.add("grape");
		list2.add("kiwi");
		list2.add("tomato");

		list3.add("apple");
		list3.add("grape");
		list3.add("mango");
		list3.add("pineapple");

		// Test for equality and non-equality
		assertTrue(list1.equals(list1));
		assertTrue(list1.equals(list2));
		assertTrue(list2.equals(list1));

		assertFalse(list3.equals(list2));
		assertFalse(list1.equals(list3));
		assertFalse(list1.equals(list4)); // null
	}

	/**
	 * Tests that hashCode works correctly.
	 */
	@Test
	public void testHashCode() {
		SortedList<String> list1 = new SortedList<String>();
		SortedList<String> list2 = new SortedList<String>();
		SortedList<String> list3 = new SortedList<String>();

		// Make two lists the same and one list different
		list1.add("pear");
		list1.add("grape");
		list1.add("kiwi");
		list1.add("tomato");

		list2.add("pear");
		list2.add("grape");
		list2.add("kiwi");
		list2.add("tomato");

		list3.add("apple");
		list3.add("grape");
		list3.add("mango");
		list3.add("pineapple");

		// Test for the same and different hashCodes
		assertEquals(list1.hashCode(), list1.hashCode());
		assertEquals(list1.hashCode(), list2.hashCode());
		assertEquals(list2.hashCode(), list1.hashCode());

		// Test for each of the fields
		assertNotEquals(list1.hashCode(), list3.hashCode());
		assertNotEquals(list3.hashCode(), list2.hashCode());

	}

}
