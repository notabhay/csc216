package edu.ncsu.csc216.pack_scheduler.io;

import java.io.*;
import java.util.*;

import edu.ncsu.csc216.collections.list.SortedList;
import edu.ncsu.csc216.pack_scheduler.user.Student;

/**
 * Class Student Record IO is a class to read and write from and to text files.
 * The read student records class utilizes the scanner class and the file reader
 * class. The read student record will throw a FileNotFound Exception if the
 * file passes does not exist. The write student records method will throw a IO
 * Exception if the file passed is not valid.
 *
 * @author Alexander Depolt
 * @author Pradhan Chetan Venkataramaiah
 * @author Abhay Sriwastawa
 */
public class StudentRecordIO {

	/**
	 * StudentRecordIO Constructor creates the Student Record IO class without any
	 * parameters
	 */
	public StudentRecordIO() {
		// Empty Constructor For Testing
	}

	/**
	 * This method creates and closes a Scanner to read a file from a text file. A
	 * line of text is parsed from the scanner and passed to the read record method
	 * for parsing and the creation of the Student object for the data.
	 * 
	 * @param fileName a file containing the student data
	 * @return studentDirectory an array of student data
	 * @throws FileNotFoundException if the file name passed to the Scanner is not
	 *                               valid.
	 */
	public static SortedList<Student> readStudentRecords(String fileName) throws FileNotFoundException {
		Scanner studentFileReader = new Scanner(new FileInputStream(fileName));
		SortedList<Student> studentDirectory = new SortedList<Student>();
		while (studentFileReader.hasNextLine()) {
			try {
				Student studentRecord = processStudent(studentFileReader.nextLine());
				studentDirectory.add(studentRecord);
			} catch (IllegalArgumentException e) {
				// skip line
			}
		}
		studentFileReader.close();
		return studentDirectory;
	}

	/**
	 * Reads a line of student data and creates a student object based on the data.
	 * The error checking is performed in the Student class.
	 *
	 * @param nextLine String that contains all information fields
	 * @return Student object containing the sudent's information
	 */
	private static Student processStudent(String nextLine) {
		/** Student's first name */
		String firstName = "";

		/** Student's last name */
		String lastName = "";

		/** Student's id */
		String id = "";

		/** Student's email */
		String email = "";

		/** Student's password */
		String hashPW = "";

		/** Student's maximum credit hours */
		int maxCredits = 0;

		Scanner stringReader = new Scanner(nextLine);
		stringReader.useDelimiter(",");
		try {
			firstName = stringReader.next();
			lastName = stringReader.next();
			id = stringReader.next();
			email = stringReader.next();
			hashPW = stringReader.next();
			maxCredits = stringReader.nextInt();
		} catch (NoSuchElementException e) {
			stringReader.close();
			throw new IllegalArgumentException();
		}
		stringReader.close();
		Student newStudent = new Student(firstName, lastName, id, email, hashPW, maxCredits);
		return newStudent;
	}

	/**
	 * Write student record creates and closes a PrintStream that prints a student
	 * directory to specified file. The text is generated through the
	 * Student.toString method call.
	 *
	 * @param fileName         the name of the file to write to
	 * @param studentDirectory the array list of student data
	 * @throws IOException if the file name passed to the PrintStream is not valid.
	 */
	public static void writeStudentRecords(String fileName, SortedList<Student> studentDirectory) throws IOException {
		PrintStream fileWriter = new PrintStream(new File(fileName));
		for (int i = 0; i < studentDirectory.size(); i++) {
			fileWriter.println(studentDirectory.get(i).toString());
		}
		fileWriter.close();
	}
}
