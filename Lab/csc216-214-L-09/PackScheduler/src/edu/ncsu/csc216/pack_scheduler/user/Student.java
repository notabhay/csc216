package edu.ncsu.csc216.pack_scheduler.user;

/**
 * The Student class represents a single student's information. The student
 * object is constructed using setter methods. The setter methods verify data
 * passed to them, they throw IllegalArgumentException if the data passed does
 * not follow the standards.
 *
 * @author Pradhan Chetan Venkataramaiah
 * @author Alexander Depolt
 * @author Abhay Sriwastawa
 */
public class Student implements Comparable<Student> {
	/** Student's first name */
	private String firstName;

	/** Student's last name */
	private String lastName;

	/** Student's id */
	private String id;

	/** Student's email */
	private String email;

	/** Student's password */
	private String password;

	/** Student's maximum credit hours */
	private int maxCredits;

	/** Max number of credits that any student may have */
	public static final int MAX_CREDITS = 18;

	/**
	 * Student Constructor constructs a Student object with values for all
	 * information fields. Method uses setter methods to verify and assign values to
	 * the private student variables.
	 *
	 * @param firstName  first name of Student
	 * @param lastName   last name of Student
	 * @param id         id of Student
	 * @param email      email of Student
	 * @param password   password for Student's account
	 * @param maxCredits max credits being taken by Student
	 */
	public Student(String firstName, String lastName, String id, String email, String password, int maxCredits) {
		setFirstName(firstName);
		setLastName(lastName);
		setId(id);
		setEmail(email);
		setPassword(password);
		setMaxCredits(maxCredits);
	}

	/**
	 * Student Constructor constructs a Student object with values for all
	 * information fields. Method uses setter methods to verify and assign values to
	 * the private student variables. This constructor is used when the student is
	 * constructed without a specified maxCredits, the maxCredits is set to 18.
	 *
	 * @param firstName first name of Student
	 * @param lastName  last name of Student
	 * @param id        id of Student
	 * @param email     email of Student
	 * @param password  password for Student's account
	 */
	public Student(String firstName, String lastName, String id, String email, String password) {
		this(firstName, lastName, id, email, password, MAX_CREDITS);
	}

	/**
	 * Method returns the Student's first name.
	 *
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the Student's first name. If the name string it is null or an empty
	 * String, an IllegalArgumentException is thrown.
	 *
	 * @param firstName the firstName to set
	 * @throws IllegalArgumentException if first name is null or an empty String
	 */
	public void setFirstName(String firstName) {
		if (firstName == null || firstName.length() == 0)
			throw new IllegalArgumentException("Invalid first name");
		this.firstName = firstName;
	}

	/**
	 * Method returns the Student's last name.
	 *
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the Student's last name. If it is null or an empty String, an
	 * IllegalArgumentException is thrown.
	 *
	 * @param lastName the lastName to set
	 * @throws IllegalArgumentException if last name is null or an empty String
	 */
	public void setLastName(String lastName) {
		if (lastName == null || lastName.length() == 0)
			throw new IllegalArgumentException("Invalid last name");
		this.lastName = lastName;
	}

	/**
	 * Method returns the Student's id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * setId sets the Student's id. If it is null or an empty String, an
	 * IllegalArgumentException is thrown.
	 *
	 * @param id the id value to be set.
	 * @throws IllegalArgumentException if id value passed, null or an empty String.
	 *
	 */

	public void setId(String id) {
		if (id == null || id.length() == 0)
			throw new IllegalArgumentException("Invalid id");
		this.id = id;
	}

	/**
	 * Return the Student's email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the Student's email. If it is null or an empty String, or fails to meet
	 * the requirements for a valid email address, an IllegalArgumentException is
	 * thrown.
	 *
	 * @param email the email to set
	 * @throws IllegalArgumentException if email is null or an empty String, or
	 *                                  fails to meet the requirements for a valid
	 *                                  email address
	 */

	public void setEmail(String email) {
		if (email == null || email.length() == 0 || !email.contains("@") || !email.contains(".")
				|| email.lastIndexOf(".") < email.indexOf("@"))
			throw new IllegalArgumentException("Invalid email");
		this.email = email;
	}

	/**
	 * Return the Student's hashed password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the Student's password. If it is null or an empty String, an
	 * IllegalArgumentException is thrown.
	 *
	 * @param password the password to set
	 * @throws IllegalArgumentException if password is null or an empty String
	 */
	public void setPassword(String password) {
		if (password == null || password.length() == 0)
			throw new IllegalArgumentException("Invalid password");
		this.password = password;
	}

	/**
	 * Return the Student's max credits.
	 *
	 * @return the maxCredits
	 */
	public int getMaxCredits() {
		return maxCredits;
	}

	/**
	 * Sets the Student's maximum credits. If it is less than 3 or greater than 18,
	 * an IllegalArgumentException is thrown.
	 *
	 * @param maxCredits the maxCredits to set
	 * @throws IllegalArgumentException if maxCredits entered is less than 3 or
	 *                                  greater than 18
	 */
	public void setMaxCredits(int maxCredits) {
		if (maxCredits < 3 || maxCredits > MAX_CREDITS)
			throw new IllegalArgumentException("Invalid max credits");
		this.maxCredits = maxCredits;
	}

	/**
	 * The maxCredits Generates a hashCode for Student using all fields.
	 *
	 * @return hashCode for Student
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + email.hashCode();
		result = prime * result + firstName.hashCode();
		result = prime * result + password.hashCode();
		result = prime * result + id.hashCode();
		result = prime * result + lastName.hashCode();
		result = prime * result + maxCredits;

		return result;

	}

	/**
	 * Compares a given object to this object for equality of all fields.
	 *
	 * @param obj the Object to compare
	 * @return true if the objects are the same on all fields
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		Student other = (Student) obj;
		if (!email.equals(other.email))
			return false;
		if (!firstName.equals(other.firstName))
			return false;
		if (!password.equals(other.password))
			return false;
		if (!id.equals(other.id))
			return false;
		if (!lastName.equals(other.lastName))
			return false;
		if (maxCredits != other.maxCredits)
			return false;
		return true;
	}

	@Override
	/**
	 * Returns string for the student object
	 */
	public String toString() {
		return firstName + "," + lastName + "," + id + "," + email + "," + password + "," + maxCredits;
	}

	/**
	 * Throw new ClassCastException if the specified objects type prevents it from
	 * being compared Compares the Last Name then First Name then user ID for which
	 * one is larger smaller or the same based on String.compare
	 */
	@Override
	public int compareTo(Student s) {
		if (s == null) {
			throw new NullPointerException("Null element cannot be added");
		}

		if (this.lastName.toUpperCase().compareTo(s.lastName.toUpperCase()) == 0) {
			if (this.firstName.toUpperCase().compareTo(s.firstName.toUpperCase()) == 0) {
				if (this.id.compareTo(s.id) == 0) {
					return 0;
				} else if (this.id.toUpperCase().compareTo(s.id.toUpperCase()) > 0) {
					return 1;
				} else {
					return -1;
				}
			} else if (this.firstName.toUpperCase().compareTo(s.firstName.toUpperCase()) > 0) {
				return 1;
			} else {
				return -1;
			}
		} else if (this.lastName.toUpperCase().compareTo(s.lastName.toUpperCase()) > 0) {
			return 1;
		}
		return -1;
	}
}
