package edu.ncsu.csc216.pack_scheduler.course;

/**
 * Course Class Constructs and Edits Course objects
 *
 * @author Abhay
 *
 */
public class Course extends Activity implements Comparable<Course> {

	/**
	 * Constant containing the length of the section String, which is a value of 3
	 */
	private static final int SECTION_LENGTH = 3;
	/**
	 * Constant containing the maximum length of the name String, which is a value
	 * of 6
	 */
	private static final int MAX_NAME_LENGTH = 6;
	/**
	 * Constant containing the minimum length of the name String, which is a value
	 * of 4
	 */
	private static final int MIN_NAME_LENGTH = 4;
	/**
	 * Constant containing the maximum number of credits for a course, which is a
	 * value of 5
	 */
	private static final int MAX_CREDITS = 5;
	/**
	 * Constant containing the minimum number of credits for a course, which is a
	 * value of 1
	 */
	private static final int MIN_CREDITS = 1;
	/** Course's name. */
	private String name;
	/** Course's section. */
	private String section;
	/** Course's credit hours */
	private int credits;
	/** Course's instructor */
	private String instructorId;

	/**
	 * Constructs a Course object with values for all fields.
	 *
	 * @param name         name of Course
	 * @param title        title of Course
	 * @param section      section of Course
	 * @param credits      credit hours for Course
	 * @param instructorId instructor's unity id
	 * @param meetingDays  meeting days for Course as series of chars
	 * @param startTime    start time for Course
	 * @param endTime      end time for Course
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays,
			int startTime, int endTime) {
		super(title, meetingDays, startTime, endTime);
		setName(name);
		setSection(section);
		setCredits(credits);
		setInstructorId(instructorId);
	}

	/**
	 * Creates a Course with the given name, title, section, credits, instructorId,
	 * and meetingDays for courses that are arranged.
	 *
	 * @param name         name of Course
	 * @param title        title of Course
	 * @param section      section of Course
	 * @param credits      credit hours for Course
	 * @param instructorId instructor's unity id
	 * @param meetingDays  meeting days for Course as series of chars
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays) {
		this(name, title, section, credits, instructorId, meetingDays, 0, 0);
	}

	/**
	 * Returns the Course's name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the Course's name. If the name is null, has a length less than 4 or
	 * greater than 6, an IllegalArgumentException is thrown.
	 *
	 * @param name the name to set
	 * @throws IllegalArgumentException if name is null or length is less than 4 or
	 *                                  greater than 6
	 */
	private void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.name = name;
	}

	/**
	 * Returns the Course's section
	 *
	 * @return the section
	 */
	public String getSection() {
		return section;
	}

	/**
	 * Sets the Course's section. If the section has a length of 3, an
	 * IllegalArgumentException is thrown.
	 *
	 * @param section the section to set
	 * @throws IllegalArgumentException if the section has a length of 3
	 */
	public void setSection(String section) {
		if (section == null) {
			throw new IllegalArgumentException();
		}
		if (section.length() != SECTION_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.section = section;
	}

	/**
	 * Returns the Course's credits
	 *
	 * @return the credits
	 */
	public int getCredits() {
		return credits;
	}

	/**
	 * Sets the Course's credits. If the credit hours are less than 1 or greater
	 * than 5 or not a number, an IllegalArgumentException is thrown.
	 *
	 * @param credits the credits to set
	 * @throws IllegalArgumentException if the credits has a length of 3
	 */
	public void setCredits(int credits) {
		if (credits < MIN_CREDITS || credits > MAX_CREDITS) {
			throw new IllegalArgumentException();
		}
		this.credits = credits;
	}

	/**
	 * Returns the Course's instructorId
	 *
	 * @return the instructorId
	 */
	public String getInstructorId() {
		return instructorId;
	}

	/**
	 * Sets the Course's instructorId. If the instructorId is null an
	 * IllegalArgumentException is thrown.
	 *
	 * @param instructorId the instructorId to set
	 * @throws IllegalArgumentException if instructorId is null
	 */
	public void setInstructorId(String instructorId) {
		if (instructorId == null) {
			throw new IllegalArgumentException();
		}
		if (instructorId.length() == 0) {
			throw new IllegalArgumentException();
		}
		this.instructorId = instructorId;
	}

//	/**
//	 * Checks if the given Activity is a duplicate of the Course by name.
//	 *
//	 * @param activity activity
//	 * @return Returns true if the given Activity is a duplicate of the Course by
//	 *         name.
//	 */
//	public boolean isDuplicate(Activity activity) {
//		if (activity instanceof Course) {
//			Course course = (Course) activity;
//			return this.getName().equals(course.getName());
//		}
//		return false;
//	}

	/**
	 * Returns a comma separated value String of all Course fields.
	 *
	 * @return String representation of Course
	 */
	@Override
	public String toString() {
		if (getMeetingDays().equals("A")) {
			return name + "," + getTitle() + "," + section + "," + credits + "," + instructorId + ","
					+ getMeetingDays();
		}
		return name + "," + getTitle() + "," + section + "," + credits + "," + instructorId + "," + getMeetingDays()
				+ "," + getStartTime() + "," + getEndTime();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + credits;
		result = prime * result + (instructorId.hashCode());
		result = prime * result + (name.hashCode());
		result = prime * result + (section.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		Course other = (Course) obj;
		if (credits != other.credits)
			return false;
		if (!instructorId.equals(other.instructorId))
			return false;
		if (!section.equals(other.section))
			return false;
		return true;
	}

	@Override
	public String[] getShortDisplayArray() {
		String[] x = { getName(), getSection(), getTitle(), getMeetingString() };
		return x;
	}

	@Override
	public String[] getLongDisplayArray() {
		String[] x = { getName(), getSection(), getTitle(), Integer.toString(getCredits()), getInstructorId(),
				getMeetingString(), "" };
		return x;
	}

	@Override
	public void setMeetingDays(String meetingDays) {
		if (meetingDays == null) {
			throw new IllegalArgumentException();
		}
		if (meetingDays.length() == 0) {
			throw new IllegalArgumentException();
		}
		if (meetingDays.contains("B") || meetingDays.contains("C") || meetingDays.contains("D")
				|| meetingDays.contains("E") || meetingDays.contains("G") || meetingDays.contains("I")
				|| meetingDays.contains("J") || meetingDays.contains("K") || meetingDays.contains("L")
				|| meetingDays.contains("N") || meetingDays.contains("O") || meetingDays.contains("P")
				|| meetingDays.contains("Q") || meetingDays.contains("R") || meetingDays.contains("S")
				|| meetingDays.contains("U") || meetingDays.contains("V") || meetingDays.contains("X")
				|| meetingDays.contains("Y") || meetingDays.contains("Z")) {
			throw new IllegalArgumentException();
		}
		if (meetingDays.contains("A") && meetingDays.length() > 1) {
			throw new IllegalArgumentException();
		}
		super.setMeetingDays(meetingDays);
	}

	@Override
	public int compareTo(Course o) {
		if (o == null) {
			throw new NullPointerException("Null element cannot be added");
		}

		if (this.name.toUpperCase().compareTo(o.name.toUpperCase()) == 0) {
			if (this.getSection().compareTo(o.getSection().toUpperCase()) == 0) {
				return 0;
			} else if (this.getSection().compareTo(o.getSection()) > 0) {
				return 1;
			} else {
				return -1;
			}
		} else if (this.name.toUpperCase().compareTo(o.name.toUpperCase()) > 0) {
			return 1;
		}
		return -1;
	}
}
