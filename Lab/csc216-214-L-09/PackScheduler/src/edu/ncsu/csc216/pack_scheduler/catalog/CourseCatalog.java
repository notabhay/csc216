package edu.ncsu.csc216.pack_scheduler.catalog;

import java.io.FileNotFoundException;
import java.io.IOException;
import edu.ncsu.csc216.collections.list.SortedList;
import edu.ncsu.csc216.pack_scheduler.course.Course;
import edu.ncsu.csc216.pack_scheduler.io.CourseRecordIO;

/**
 * CourseCatalog Class Creates new Course Catalogs by loading them from a file specified by the user.
 * It can also add, remove, get and export courses to and from the catalog
 *
 * @author Alexander
 * @author Abhay
 * @author Pradhan
 */
public class CourseCatalog {

	/** Course Catalog */
	private SortedList<Course> catalog;

    /**
     * Course Catalog Constructor creates the new sorted list
     */
	CourseCatalog() {
		catalog = new SortedList<Course>();
	}

    /**
     * Creates a new Course Catalog
     */
	public void newCourseCatalog() {
		catalog = new SortedList<Course>();
	}

    /**
     * Loads Courses into the catalog from an external file specified by the user
     *
     * @param fileName fileName to be loaded
     */
	public void loadCoursesFromFile(String fileName) {
		catalog = new SortedList<Course>();
		try {
			catalog = CourseRecordIO.readCourseRecords(fileName);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Cannot find file.");
		}
	}

	/**
     * Checks for and Adds Courses to Schedule from Catalog matching the given name and section
     *
     * @param  name name
     * @param  title title
     * @param  section section
     * @param  credits credits
     * @param  instructorId instructorId
     * @param  meetingDays meetingDays
     * @param  startTime startTime
     * @param  endTime endTime
     * @return returns true if course has been added and false otherwise
     */
	public boolean addCourseToCatalog(String name, String title, String section, int credits, String instructorId,
			String meetingDays, int startTime, int endTime) {
		Course x = null;
		try {
			x = new Course(name, title, section, credits, instructorId, meetingDays, startTime, endTime);
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("The course cannot be added due to an error during creation.");
		}

		Course y = null;
		if (catalog.size() > 0) {
			for (int i = 0; i < catalog.size(); i++) {
				y = catalog.get(i);
				if (y.getTitle().contentEquals(title) && y.getSection().contentEquals(section)) {
					return false;
				}
			}
		}

		catalog.add(x);
		return true;

	}

    /**
     * Removes Courses from the catalog with specified name and section
     *
     * @param  name    name
     * @param  section section
     * @return         returns false is course cannot be removed and true if specified course has been removed
     */
	public boolean removeCourseFromCatalog(String name, String section) {
		Course x = null;

		for (int i = 0; i < catalog.size(); i++) {
			x = catalog.get(i);
			if (x.getName().equals(name) && x.getSection().equals(section)) {
				catalog.remove(i);
				return true;
			}
		}
		return false;
	}

    /**
     * Gets a course from the catalog from the name and section provided by the user
     *
     * @param  name name
     * @param  section section
     * @return returns the added course or null if the course was null
     */
	public Course getCourseFromCatalog(String name, String section) {
		Course x = null;

		for (int i = 0; i < catalog.size(); i++) {
			x = catalog.get(i);
			if (x.getName().equals(name) && x.getSection().equals(section)) {
				return x;
			}
		}
		return null;
	}

    /**
     * Gets the full course catalog in a 2d array
     *
     * @return returns the full course catalog in a 2d array
     */
	public String[][] getCourseCatalog() {
		Course x = null;
		String[][] y = new String[catalog.size()][4];
		for (int i = 0; i < catalog.size(); i++) {
			x = catalog.get(i);
			String[] a = x.getShortDisplayArray();
			for (int j = 0; j < 4; j++) {
				y[i][j] = a[j];
			}
		}
		return y;
	}

    /**
     * Saves the course catalog in an external file with the fileName specified by the user
     *
     * @param fileName fileName
     */
	public void saveCourseCatalog(String fileName) {
		try {
			CourseRecordIO.writeCourseRecords(fileName, catalog);
		} catch (IOException e) {
			throw new IllegalArgumentException("The file cannot be saved.");
		}
	}
}
