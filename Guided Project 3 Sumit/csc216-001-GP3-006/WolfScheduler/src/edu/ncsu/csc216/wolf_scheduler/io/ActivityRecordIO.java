/**
 * 
 */
package edu.ncsu.csc216.wolf_scheduler.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import edu.ncsu.csc216.wolf_scheduler.course.Activity;

/** Reads Activity records from text files. Writes a set of ActivityRecords to a
 * file.
 * @author Sumit Biswas
 *
 */
public class ActivityRecordIO {

	/**
	 * Writes the given list of Activities to a specified file.
	 * 
	 * @param fileName is the name of the file to which the courses are to be written.
	 * @param courses is the ArrayList of Activities.
	 * @throws IOException if there is an error with writing to the specified file.
	 */
	public static void writeActivityRecords(String fileName, ArrayList<Activity> courses) throws IOException {
		PrintStream fileWriter = new PrintStream(new File(fileName));
	
		for (int i = 0; i < courses.size(); i++) {
		    fileWriter.println(courses.get(i).toString());
		}
	
		fileWriter.close();
	}

}
