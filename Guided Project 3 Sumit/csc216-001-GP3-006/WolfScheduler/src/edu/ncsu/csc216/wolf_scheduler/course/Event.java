/**
 * 
 */
package edu.ncsu.csc216.wolf_scheduler.course;

/** Event Class that represents an Event object.
 * @author Sumit Biswas
 *
 */
public class Event extends Activity {

	/** Integer that describes how many weeks between events. */
	private int weeklyRepeat;
	/** Field that stores details about the event. */
	private String eventDetails;

	/** Constructs an Event object with values for all fields.
	 * 
	 * @param title        title of Event.
	 * @param meetingDays  meeting days for Event as series of chars.
	 * @param startTime    start time for Event.
	 * @param endTime      end time for Event.
	 * @param weeklyRepeat number of weeks between events.
	 * @param eventDetails details for the event.
	 */
	public Event(String title, String meetingDays, int startTime, int  endTime, int weeklyRepeat, String eventDetails) {
		super(title, meetingDays, startTime, endTime);
		setWeeklyRepeat(weeklyRepeat);
		setEventDetails(eventDetails);
	}

	/** Accessor method for weeklyRepeat, the number of weeks between events.
	 * @return returns the weeklyRepeat.
	 */
	public int getWeeklyRepeat() {
		return weeklyRepeat;
	}

	/** Mutator method for weeklyRepeat, the number of weeks between events.
	 * @param weeklyRepeat the number of weeks between events.
	 * @throws IllegalArgumentException if the weekly repeat is invalid.
	 */
	public void setWeeklyRepeat(int weeklyRepeat) {
		if (weeklyRepeat < 1 || weeklyRepeat > 4) {
			throw new IllegalArgumentException();
		}
		this.weeklyRepeat = weeklyRepeat;
	}

	/** Method that returns a String with meeting timing details.
	 * @return returns a String with the details.
	 */
	@Override
	public String getMeetingString() {
		// TODO Auto-generated method stub
		return super.getMeetingString() + " (every " + getWeeklyRepeat() + " weeks)";
	}

	/** Method that returns a String all the information about the event.
	 * @return returns the String with the information.
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getTitle() + "," + getMeetingDays() + "," + getStartTime() + "," + getEndTime() +
				"," + getWeeklyRepeat() + "," + getEventDetails();
	}

	/** Accessor method for event details.
	 * @return the eventDetails.
	 */
	public String getEventDetails() {
		return eventDetails;
	}

	/** Mutator method for event details.
	 * @param eventDetails the eventDetails to set.
	 * @throws IllegalArgumentException if the event details are invalid.
	 */
	public void setEventDetails(String eventDetails) {
		if (eventDetails == null) {
			throw new IllegalArgumentException("Invalid event details");
		}
		this.eventDetails = eventDetails;
	}
	
	/**
	 * Override mutator method for meetingDays field.
	 * @param meetingDays the meetingDays to set.
	 * @throws IllegalArgumentException if the days are invalid.
	 */
	@Override
	public void setMeetingDays(String meetingDays) {
		String valid = "MTWHFUS";
		if (meetingDays == null || meetingDays.equals("")) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < meetingDays.length(); i++) {
			char ch = meetingDays.charAt(i);
			boolean correct = false;
			for (int j = 0; j < valid.length(); j++) {
				if (ch == valid.charAt(j)) {
					correct = true;
				}
			}
			if (!correct) {
				throw new IllegalArgumentException();
			}
		}
		super.setMeetingDays(meetingDays);
	}
	
	/** Method that checks if the given Activity is an Event
	 * and if the Event is duplicate of an Event in the Schedule.
	 * @param activity is the Activity being compared.
	 * @return returns a boolean to indicate the result.
	 */
	public boolean isDuplicate(Activity activity) {
		if (activity instanceof Event) {
			Event event = (Event) activity;
			return this.getTitle().equals(event.getTitle());
		} 
		return false;
	}
	
	/** Method that returns a String array with two empty strings and the Event title
	 * and meeting days String.
	 * @return returns the String array.
	 */
	public String[] getShortDisplayArray() {
		String[] sda = {"", "", getTitle(), getMeetingString()};
		return sda;
	}

	/** Method that returns a String array with the Course name, section, title, credits, 
	 * instructor id and meeting days.
	 * @return returns the String array.
	 */
	public String[] getLongDisplayArray() {
		String[] lda = {"", "", getTitle(), "", "", getMeetingString(), getEventDetails()};
		return lda;
	}

}
