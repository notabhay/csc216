/**
 * 
 */
package edu.ncsu.csc216.wolf_scheduler.course;

/**
 * Course Class that represents the Course object.
 * 
 * @author Sumit Biswas
 *
 */
public class Course extends Activity {

	/** Course's name. */
	private String name;
	/** Course's section. */
	private String section;
	/** Course's credit hours */
	private int credits;
	/** Course's instructor */
	private String instructorId;
	/** Constant for section length. */
	private static final int SECTION_LENGTH = 3;
	/** Constant for maximum name length. */
	private static final int MAX_NAME_LENGTH = 6;
	/** Constant for minimum name length. */
	private static final int MIN_NAME_LENGTH = 4;
	/** Constant for maximum credits. */
	private static final int MAX_CREDITS = 5;
	/** Constant for minimum credits. */
	private static final int MIN_CREDITS = 1;
	/**
	 * Constructs a Course object with values for all fields.
	 * 
	 * @param name         name of Course.
	 * @param title        title of Course.
	 * @param section      section of Course.
	 * @param credits      credit hours for Course.
	 * @param instructorId instructor's unity id.
	 * @param meetingDays  meeting days for Course as series of chars.
	 * @param startTime    start time for Course.
	 * @param endTime      end time for Course.
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays,
			int startTime, int endTime) {
		super(title, meetingDays, startTime, endTime);
		setName(name);
		setSection(section);
		setCredits(credits);
		setInstructorId(instructorId);
	}

	/**
	 * Alternative constructor for the Course object.
	 * This constructor calls the main constructor with default values 
	 * passed in for meeting times.
	 * @param name         name of Course.
	 * @param title        title of Course.
	 * @param section      section of Course.
	 * @param credits      credit hours for Course.
	 * @param instructorId instructor's unity id.
	 * @param meetingDays  meeting days for Course as series of chars.
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays) {
		this(name, title, section, credits, instructorId, meetingDays, 0, 0);
	}

	/**
	 * Accessor method for name.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Mutator method for name field.
	 * @param name the name to set.
	 * @throws IllegalArgumentException if the name is invalid
	 */
	private void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
			throw new IllegalArgumentException();
		}
		this.name = name;
	}

	/**
	 * Accessor method for section.
	 * 
	 * @return the section.
	 */
	public String getSection() {
		return section;
	}

	/**
	 * Mutator method for the section field. 
	 * @param section the section to set.
	 * @throws IllegalArgumentException if the section is invalid.
	 */
	public void setSection(String section) {
		if (section == null || section.length() != SECTION_LENGTH) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < section.length(); i++) {
			char ch = section.charAt(i);
			if (!Character.isDigit(ch)) {
				throw new IllegalArgumentException();
			}
		}
		this.section = section;
	}

	/**
	 * Accessor method for credits.
	 * 
	 * @return the credits.
	 */
	public int getCredits() {
		return credits;
	}

	/**
	 * Mutator method for credits field.
	 * @param credits the credits to set.
	 * @throws IllegalArgumentException if the credits are invalid.
	 */
	public void setCredits(int credits) {
		if (credits < MIN_CREDITS || credits > MAX_CREDITS) {
			throw new IllegalArgumentException();
		}
		this.credits = credits;
	}

	/**
	 * Accessor method for instructorId.
	 * 
	 * @return the instructorId.
	 */
	public String getInstructorId() {
		return instructorId;
	}

	/**
	 * Mutator method for instructorId field.
	 * @param instructorId the instructorId to set.
	 * @throws IllegalArgumentException if the instructor id is invalid.
	 */
	public void setInstructorId(String instructorId) {
		if (instructorId == null || instructorId.equals("")) {
			throw new IllegalArgumentException();
		}
		this.instructorId = instructorId;
	}
	
	/**
	 * Override mutator method for meetingDays field. 
	 * @param meetingDays the meetingDays to set.
	 * @throws IllegalArgumentException if the days are invalid.
	 */
	@Override
	public void setMeetingDays(String meetingDays) {
		String valid = "MTWHFA";
		if (meetingDays == null || meetingDays.equals("")) {
			throw new IllegalArgumentException();
		}
		if (meetingDays.contains("A") && meetingDays.length() > 1) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < meetingDays.length(); i++) {
			char ch = meetingDays.charAt(i);
			boolean correct = false;
			for (int j = 0; j < valid.length(); j++) {
				if (ch == valid.charAt(j)) {
					correct = true;
				}
			}
			if (!correct) {
				throw new IllegalArgumentException();
			}
		}
		super.setMeetingDays(meetingDays);
	}
	
	/** Method that checks if the given Activity is a Course
	 * and if the Course is duplicate of a Course in the Schedule.
	 * @param activity is the Activity being compared.
	 * @return returns a boolean to indicate the result.
	 */
	public boolean isDuplicate(Activity activity) {
		if (activity instanceof Course) {
			Course course = (Course) activity;
			return this.getName().equals(course.getName());
		} 
		return false;
	}

	/**
	 * Generates a hashCode for Course using all fields.
	 * 
	 * @return hashCode for Course
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + credits;
		result = prime * result + ((instructorId == null) ? 0 : instructorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		return result;
	}

	/**
	 * Compares a given object to this object for equality on all fields.
	 * 
	 * @param obj the Object to compare.
	 * @return true if the objects are the same on all fields.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (credits != other.credits)
			return false;
		if (instructorId == null) {
			if (other.instructorId != null)
				return false;
		} else if (!instructorId.equals(other.instructorId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		return true;
	}

	/**
	 * Returns a comma separated value String of all Course fields.
	 * 
	 * @return String representation of Course.
	 */
	@Override
	public String toString() {
		if (getMeetingDays().equals("A")) {
			return name + "," + getTitle() + "," + section + "," + credits + "," + instructorId + "," + getMeetingDays();
		}
		return name + "," + getTitle() + "," + section + "," + credits + "," + instructorId + "," + getMeetingDays() + ","
				+ getStartTime() + "," + getEndTime();
	}

	/** Starts the program.
	 * @param args command line.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	/** Method that returns a String array with the Course name, section, title
	 * and meeting days String.
	 * @return returns the String array.
	 */
	public String[] getShortDisplayArray() {
		String[] sda = {getName(), getSection(), getTitle(), getMeetingString()};
		return sda;
	}

	/** Method that returns a String array with the Course name, section, title, credits, 
	 * instructor id, meetingDays and an empty String.
	 * @return returns the String array.
	 */
	public String[] getLongDisplayArray() {
		String[] lda = {getName(), getSection(), getTitle(), Integer.toString(getCredits()), getInstructorId(), getMeetingString(), ""};
		return lda;
	}

}
