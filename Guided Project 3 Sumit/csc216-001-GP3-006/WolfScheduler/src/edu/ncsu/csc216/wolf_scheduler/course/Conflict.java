/**
 *
 */
package edu.ncsu.csc216.wolf_scheduler.course;

/** Interface that defines the conflict behavior.
 * @author Sumit Biswas
 *
 */
public interface Conflict {

	/** Method that throws the checked ConflictException if the given Activity conflicts with another Activity.
	 *
	 * @param possibleConflictingActivity is the Activity being checked for possible conflict.
	 * @throws ConflictException is the exception thrown if there is a conflict between two Activities.
	 */
	void checkConflict(Activity possibleConflictingActivity) throws ConflictException;



}
