package edu.ncsu.csc216.wolf_scheduler.course;

/** Activity Super Class representing an Activity object.
 *
 * @author Sumit Biswas
 *
 */
public abstract class Activity implements Conflict {

	/** Course's title. */
	private String title;
	/** Course's meeting days */
	private String meetingDays;
	/** Course's starting time */
	private int startTime;

	/** Method that checks if there is a schedule conflict between two Activities and throws an exception if there is one.
	 * @throws ConflictException if there is a schedule conflict between two activities.
	 */
	@Override
	public void checkConflict(Activity possibleConflictingActivity) throws ConflictException {
		if (this.getMeetingDays().contains("A") || possibleConflictingActivity.getMeetingDays().contains("A")) {
			return;
		}
		boolean commonDays = false;
		for (int i = 0; i < this.getMeetingDays().length(); i++) {
			String check = "";
			check += this.getMeetingDays().charAt(i);
			if (possibleConflictingActivity.getMeetingDays().contains(check)) {
				commonDays = true;
			}
			check = "";
		}
		if (commonDays) {
			int tst = this.getStartTime();
			int tet = this.getEndTime();
			int pcast = possibleConflictingActivity.getStartTime();
			int pcaet = possibleConflictingActivity.getEndTime();
			if (tst <= pcast && pcast <= tet) {
				throw new ConflictException();
			} else if (pcast <= tst && tst <= pcaet) {
				throw new ConflictException();
			}
		}
	}

	/** Course's ending time */
	private int endTime;
	/** Constant for upper limit of military time. */
	private static final int UPPER_TIME = 2400;
	/** Constant for noon in military time. */
	private static final int HALF_TIME = 1200;
	/** Constant for upper limit of minutes in an hour. */
	private static final int UPPER_HOUR = 60;
	/** Constant for the hour of noon. */
	private static final int HALF_DAY = 12;
	/** Constant for number used as a divisor or for comparison. */
	private static final int HUNDRED = 100;
	/** Constant for number used as a divisor or for comparison. */
	private static final int THOUSAND = 1000;

	/** Constructs an Activity object with values for all fields.
	 *
	 * @param title        title of Activity.
	 * @param meetingDays  meeting days for Activity as series of chars.
	 * @param startTime    start time for Activity.
	 * @param endTime      end time for Activity.
	 */
	public Activity(String title, String meetingDays, int startTime, int endTime) {
		setTitle(title);
		setMeetingDays(meetingDays);
		setActivityTime(startTime, endTime);
	}

	/**
	 * Accessor method for title.
	 *
	 * @return the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Mutator method for title field.
	 *
	 * @param title the title to set.
	 * @throws IllegalArgumentException if the title is invalid.
	 */
	public void setTitle(String title) {
		if (title == null || title.equals("")) {
			throw new IllegalArgumentException();
		}
		this.title = title;
	}

	/**
	 * Accessor method for meetingDays.
	 *
	 * @return the meetingDays.
	 */
	public String getMeetingDays() {
		return meetingDays;
	}

	/**
	 * Mutator method for meetingDays field.
	 *
	 * @param meetingDays the meetingDays to set.
	 */
	public void setMeetingDays(String meetingDays) {
		this.meetingDays = meetingDays;
	}

	/**
	 * Accessor method for startTime.
	 *
	 * @return the startTime
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
	 * Accessor method for endTime.
	 *
	 * @return the endTime
	 */
	public int getEndTime() {
		return endTime;
	}

	/**
	 * Method that sets Activity time.
	 *
	 * @param startTime is the start time.
	 * @param endTime   is the end time.
	 * @throws IllegalArgumentException if the start time/end time is invalid.
	 */
	public void setActivityTime(int startTime, int endTime) {
		if (meetingDays.equals("A") && startTime > 0 || meetingDays.equals("A") && endTime > 0) {
			throw new IllegalArgumentException();
		}
		if (startTime > UPPER_TIME - 1 || startTime < 0 || endTime > UPPER_TIME - 1 || endTime < 0) {
			throw new IllegalArgumentException();
		}
		if (endTime < startTime) {
			throw new IllegalArgumentException();
		}
		int check = startTime;
		while (check > HUNDRED - 1) {
			check %= HUNDRED;
		}
		if (check > UPPER_HOUR - 1) {
			throw new IllegalArgumentException();
		}
		check = endTime;
		while (check > HUNDRED - 1) {
			check %= HUNDRED;
		}
		if (check > UPPER_HOUR - 1) {
			throw new IllegalArgumentException();
		}
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/** Method that returns a String with the meeting days and time.
	 * @return returns the String with the information.
	 */
	public String getMeetingString() {
		String result = "";
		if (meetingDays.equals("A")) {
			result += "Arranged";
			return result;
		}
		result += getMeetingDays() + " " + convertTime(startTime) + "-" + convertTime(endTime);
		return result;
	}

	/**
	 * Method that converts military time to standard format time.
	 *
	 * @param militaryTime is the time in military format.
	 * @return returns the time in standard format.
	 */
	private String convertTime(int militaryTime) {
		String converted = "";
		if (militaryTime == 0) {
			return "12:00AM";
		}
		String time = "";
		if (militaryTime < HUNDRED) {
			time += "00";
		} else if (militaryTime > HUNDRED - 1 && militaryTime < THOUSAND) {
			time += "0";
		}
		time += Integer.toString(militaryTime);
		String h = time.substring(0, 2);
		int hour = Integer.parseInt(h);
		int minutes = militaryTime;
		if (hour > HALF_DAY) {
			hour %= HALF_DAY;
		} else if (hour == 0) {
			hour = HALF_DAY;
		}
		while (minutes > HUNDRED - 1) {
			minutes %= HUNDRED;
		}
		if (militaryTime < HALF_TIME) {
			if (minutes == 0) {
				converted += hour + ":" + minutes + "0AM";
			} else {
				converted += hour + ":" + minutes + "AM";
			}
		} else {
			if (minutes == 0) {
				converted += hour + ":" + minutes + "0PM";
			} else {
				converted += hour + ":" + minutes + "PM";
			}
		}
		return converted;
	}

	/** Abstract method that checks if a given activity is a duplicate of an activity
	 * in the schedule.
	 * @param activity is the activity being checked.
	 * @return returns a boolean to indicate the result.
	 */
	public abstract boolean isDuplicate(Activity activity);

	/**
	 * Generates a hashCode for Course using all fields.
	 *
	 * @return hashCode for Course
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + endTime;
		result = prime * result + ((meetingDays == null) ? 0 : meetingDays.hashCode());
		result = prime * result + startTime;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * Compares a given object to this object for equality on all fields.
	 *
	 * @param obj the Object to compare.
	 * @return true if the objects are the same on all fields.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Activity other = (Activity) obj;
		if (endTime != other.endTime)
			return false;
		if (meetingDays == null) {
			if (other.meetingDays != null)
				return false;
		} else if (!meetingDays.equals(other.meetingDays))
			return false;
		if (startTime != other.startTime)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/** Abstract method that returns String array with some information about the Activity.
	 * Different implementations present in Course and Event Classes.
	 * @return String array with the information.
	 */
	public abstract String[] getShortDisplayArray();

	/** Abstract method that returns String array with some information about the Activity.
	 * Different implementations present in Course and Event Classes.
	 * @return String array with the information.
	 */
	public abstract String[] getLongDisplayArray();

}
