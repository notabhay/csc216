package edu.ncsu.csc216.wolf_scheduler.io;

import java.io.*;
import java.util.*;

import edu.ncsu.csc216.wolf_scheduler.course.Course;

/**
 * Reads Course records from text files. Writes a set of CourseRecords to a
 * file.
 * 
 * @author Sumit Biswas
 */
public class CourseRecordIO {
	
	/** Constant used to refer to an index of an array. */
	private static final int POSITION = 6;
	/** Constant for the length of the array storing course information. */
	private static final int LENGTH = 8;

	/**
	 * Reads course records from a file and generates a list of valid Courses. Any
	 * invalid Courses are ignored. If the file to read cannot be found or the
	 * permissions are incorrect a File NotFoundException is thrown.
	 * 
	 * @param fileName file to read Course records from.
	 * @return a list of valid Courses.
	 * @throws FileNotFoundException if the file cannot be found or read.
	 */
	public static ArrayList<Course> readCourseRecords(String fileName) throws FileNotFoundException {
	    Scanner fileReader = new Scanner(new FileInputStream(fileName));
	    ArrayList<Course> courses = new ArrayList<Course>();
	    while (fileReader.hasNextLine()) {
	        try {
	            Course course = readCourse(fileReader.nextLine());
	            boolean duplicate = false;
	            for (int i = 0; i < courses.size(); i++) {
	                Course c = courses.get(i);
	                if (course.getName().equals(c.getName()) &&
	                        course.getSection().equals(c.getSection())) {
	                    //it's a duplicate
	                    duplicate = true;
	                }
	            }
	            if (!duplicate) {
	                courses.add(course);
	            }
	        } catch (IllegalArgumentException e) {
	            //skip the line
	        }
	    }
	    fileReader.close();
	    return courses;
	}
	
	/** Method that scans a line of text for Course information and constructs and returns
	 * a Course object as specified.
	 * @param input is the line to be analyzed for Course information.
	 * @return returns a Course object constructed with the given information.
	 */
	private static Course readCourse(String input) {
		Course course = null;
		Scanner lineScanner = new Scanner(input);
		lineScanner.useDelimiter(",");
		String[] tokens = new String[LENGTH];
		int index = 0;
		try {
			while (lineScanner.hasNext()) {
				tokens[index] = lineScanner.next();
				index++;
			}
			int credits = Integer.parseInt(tokens[3]);
			if (tokens[LENGTH - 2] == null) {
				course = new Course(tokens[0], tokens[1], tokens[2], credits, tokens[4], tokens[5]);
			} else {
				int startTime = 0;
				int endTime = 0;
				String sTime = tokens[POSITION];
				String eTime = tokens[POSITION + 1];
				if (sTime.length() == 4 && sTime.charAt(0) == '0') {
					sTime = sTime.substring(1);
					startTime = Integer.parseInt(sTime);
				} else {
					startTime = Integer.parseInt(sTime);
				}
				if (eTime.length() == 4 && eTime.charAt(0) == '0') {
					eTime = eTime.substring(1);
					endTime = Integer.parseInt(eTime);
				} else {
					endTime = Integer.parseInt(eTime);
				}
				course = new Course(tokens[0], tokens[1], tokens[2], credits, tokens[4], tokens[5], startTime, endTime);
			}
		} 
		catch (NullPointerException e) {
			lineScanner.close();
			throw new IllegalArgumentException();
		}
		lineScanner.close();
		return course;
	}

}