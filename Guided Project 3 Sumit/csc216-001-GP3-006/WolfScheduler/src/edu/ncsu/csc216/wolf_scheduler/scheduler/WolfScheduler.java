/**
 * 
 */
package edu.ncsu.csc216.wolf_scheduler.scheduler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import edu.ncsu.csc216.wolf_scheduler.course.Activity;
import edu.ncsu.csc216.wolf_scheduler.course.ConflictException;
import edu.ncsu.csc216.wolf_scheduler.course.Course;
import edu.ncsu.csc216.wolf_scheduler.course.Event;
import edu.ncsu.csc216.wolf_scheduler.io.ActivityRecordIO;
import edu.ncsu.csc216.wolf_scheduler.io.CourseRecordIO;

/**
 * WolfScheduler class that represents a schedule object.
 * 
 * @author Sumit Biswas
 */
public class WolfScheduler {

	/** Title for the schedule. */
	private String title;
	/** ArrayList of Courses that make up a student's schedule. */
	private ArrayList<Activity> schedule;
	/** ArrayList of Courses that make up the course catalog. */
	private ArrayList<Course> catalog;

	/**
	 * Constructor that populates the catalog of courses from a given file and
	 * creates an empty schedule ArrayList.
	 * 
	 * @param courseRecordsFileName is the file from which the catalog of courses is
	 *                              populated.
	 * @throws IllegalArgumentException if the file cannot be found.
	 */
	public WolfScheduler(String courseRecordsFileName) {
		catalog = new ArrayList<Course>();
		schedule = new ArrayList<Activity>();
		setTitle("My Schedule");
		try {
			catalog = CourseRecordIO.readCourseRecords(courseRecordsFileName);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Cannot find file.");
		}
	}

	/**
	 * Method that returns a 2d array containing each course in the catalog with
	 * it's name, section and title.
	 * 
	 * @return returns the array with the information.
	 */
	public String[][] getCourseCatalog() {
		Course course = null;
		String[][] cat = new String[catalog.size()][4];
		for (int i = 0; i < catalog.size(); i++) {
			course = catalog.get(i);
			cat[i][0] = course.getName();
			cat[i][1] = course.getSection();
			cat[i][2] = course.getTitle();
			cat[i][3] = course.getMeetingString();
		}
		return cat;
	}

	/**
	 * Method that returns a 2d array containing each activity in the schedule with
	 * it's name, section and title and meeting string in case of a Course,
	 * or it's title and meeting string in case of an Event.
	 * 
	 * @return returns the array with the information.
	 */
	public String[][] getScheduledActivities() {
		Activity activity = null;
		String[][] sch = new String[schedule.size()][];
		for (int i = 0; i < schedule.size(); i++) {
			activity = schedule.get(i);
			sch[i] = activity.getShortDisplayArray();
		}
		return sch;
	}

	/**
	 * Method that returns a 2d array containing each activity in the schedule with
	 * it's name, section, title, credits, instructor id and meeting string in case of a Course,
	 * and it's title, meeting string and event details in case of an Event.
	 * 
	 * @return returns the array with the information.
	 */
	public String[][] getFullScheduledActivities() {
		Activity activity = null;
		String[][] fullSch = new String[schedule.size()][];
		for (int i = 0; i < schedule.size(); i++) {
			activity = schedule.get(i);
			fullSch[i] = activity.getLongDisplayArray();
		}
		return fullSch;
	}

	/**
	 * Accessor method for the title of the schedule.
	 * 
	 * @return returns the title of the schedule.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Method that exports schedule to the specified file.
	 * 
	 * @param fileName is the name of the file.
	 * @throws IllegalArgumentException if the file cannot be written to.
	 */
	public void exportSchedule(String fileName) {
		try {
			ActivityRecordIO.writeActivityRecords(fileName, schedule);
		} catch (IOException e) {
			throw new IllegalArgumentException("The file cannot be saved.");
		}

	}

	/**
	 * Method that uses the name and section to find a course in the catalog and
	 * returns it.
	 * 
	 * @param name    is the name of the course.
	 * @param section is the section of the course.
	 * @return returns the Course object with the specified name and section.
	 */
	public Course getCourseFromCatalog(String name, String section) {
		Course course = null;
		for (int i = 0; i < catalog.size(); i++) {
			course = catalog.get(i);
			if (course.getName().equals(name) && course.getSection().equals(section)) {
				return course;
			}
		}
		return null;
	}

	/**
	 * Method that finds a specified course from the catalog and adds it to the
	 * student's schedule.
	 * 
	 * @param name    is the name of the course.
	 * @param section is the section of the course.
	 * @throws IllegalArgumentException if the course has already been enrolled in.
	 * @return returns a boolean to indicate if the action was a success or not.
	 */
	public boolean addCourse(String name, String section) {
		Course course = getCourseFromCatalog(name, section);
		if (course == null) {
			return false;
		}
		Activity activity = null;
		for (int i = 0; i < schedule.size(); i++) {
			activity = schedule.get(i);
			if (course.isDuplicate(activity)) {
				throw new IllegalArgumentException("You are already enrolled in " + name);
			}
			try {
				activity.checkConflict(course);
			} catch (ConflictException e) {
				throw new IllegalArgumentException("The course cannot be added due to a conflict.");
			}
		}
		schedule.add(course);
		return true;
	}
	
	/** Constructs an Event object and adds it to the schedule unless it is a duplicate of an
	 * Event already in the schedule.
	 * 
	 * @param eventTitle        is the event Title.
	 * @param eventMeetingDays  meeting days for Event as series of chars.
	 * @param eventStartTime    start time for event.
	 * @param eventEndTime      end time for event.
	 * @param eventWeeklyRepeat number of weeks between events.
	 * @param eventDetails      details about the event.
	 * @throws IllegalArgumentException if an event with the same title has been added to the schedule already.
	 * @return returns a boolean to indicate whether the event was added to the schedule or not.
	 */
	public boolean addEvent(String eventTitle, String eventMeetingDays, int eventStartTime, int eventEndTime, int eventWeeklyRepeat, String eventDetails) {
		Event event = new Event(eventTitle, eventMeetingDays, eventStartTime, eventEndTime, eventWeeklyRepeat, eventDetails);
		Activity activity = null;
		for (int i = 0; i < schedule.size(); i++) {
			activity = schedule.get(i);
			if (event.isDuplicate(activity)) {
				throw new IllegalArgumentException("You have already created an event called " + eventTitle);
			}
			try {
				activity.checkConflict(event);
			} catch (ConflictException e) {
				throw new IllegalArgumentException("The event cannot be added due to a conflict.");
			}
		}
		schedule.add(event);
		return true;
	}

	/**
	 * Method that removes a specific activity from the student's schedule.
	 * @param idx is the index of the Activity to be removed.
	 * 
	 * @return returns if the action was a success.
	 */
	public boolean removeActivity(int idx) {
		if (idx < 0 || idx > schedule.size() - 1) {
			return false;
		}
		schedule.remove(idx);
		return true;
	}

	/**
	 * Method that resets the schedule to an empty ArrayList.
	 */
	public void resetSchedule() {
		schedule = new ArrayList<Activity>();
	}

	/**
	 * Method that sets the title of the schedule to the given title.
	 * 
	 * @param title is the given title.
	 * @throws IllegalArgumentException if title is invalid.
	 */
	public void setTitle(String title) {
		if (title == null || title.equals("")) {
			throw new IllegalArgumentException("Title cannot be null.");
		}
		this.title = title;

	}

}
