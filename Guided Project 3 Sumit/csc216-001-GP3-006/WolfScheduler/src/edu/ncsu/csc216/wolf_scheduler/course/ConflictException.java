/**
 *
 */
package edu.ncsu.csc216.wolf_scheduler.course;

/** Custom Exception Class that defines an exception which is thrown
 * when there is a conflict between two Activities.
 * @author Sumit Biswas
 *
 */
public class ConflictException extends Exception {

	/** ID used for serialization. */
	private static final long serialVersionUID = 1L;

	/** Constructs a ConflictException with a custom message.
	 * @param message is the message to be delivered when the exception is thrown.
	 */
	public ConflictException(String message) {
		super(message);
	}

	/** Constructs a Conflict exception with the default message. */
	public ConflictException() {
		this("Schedule conflict.");
	}


}
