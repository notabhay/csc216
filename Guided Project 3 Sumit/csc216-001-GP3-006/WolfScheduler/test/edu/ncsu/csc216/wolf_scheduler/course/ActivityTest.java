/**
 *
 */
package edu.ncsu.csc216.wolf_scheduler.course;

import static org.junit.Assert.*;

import org.junit.Test;

/** Test Class that tests Conflict checking functionality of the Activity Class.
 * @author Sumit Biswas
 *
 */
public class ActivityTest {

	/**
	 * Test method for {@link edu.ncsu.csc216.wolf_scheduler.course.Activity#checkConflict(edu.ncsu.csc216.wolf_scheduler.course.Activity)}.
	 */
	@Test
	public void testCheckConflict() {
		Activity a1 = new Course("CSC216", "Programming Concepts - Java", "001", 4, "sesmith5", "MW", 1330, 1445);
		Activity a2 = new Course("CSC216", "Programming Concepts - Java", "001", 4, "sesmith5", "TH", 1330, 1445);
		try {
			a1.checkConflict(a2);
			assertEquals("Incorrect meeting string for this Activity.", "MW 1:30PM-2:45PM", a1.getMeetingString());
			assertEquals("Incorrect meeting string for possibleConflictingActivity.", "TH 1:30PM-2:45PM", a2.getMeetingString());
		} catch (ConflictException e) {
			fail("A ConflictException was thrown when two Activities at the same time on completely distinct days were compared.");
		}

		// Same values but with the parameter and 'this' switched to make sure that the method is commutative.
		try {
			a2.checkConflict(a1);
			assertEquals("Incorrect meeting string for this Activity.", "MW 1:30PM-2:45PM", a1.getMeetingString());
			assertEquals("Incorrect meeting string for possibleConflictingActivity.", "TH 1:30PM-2:45PM", a2.getMeetingString());
		} catch (ConflictException e) {
			fail("A ConflictException was thrown when two Activities at the same time on completely distinct days were compared.");
		}

		//Update a1 with the same meeting days and a start time that overlaps the end time of a2
		a1.setMeetingDays("TH");
		a1.setActivityTime(1445, 1530);
		try {
			a1.checkConflict(a2);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("TH 2:45PM-3:30PM", a1.getMeetingString());
			assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
		}
		// Same updated values but with the parameter and 'this' switched to make sure that the method is commutative.
		try {
			a2.checkConflict(a1);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("TH 2:45PM-3:30PM", a1.getMeetingString());
			assertEquals("TH 1:30PM-2:45PM", a2.getMeetingString());
		}
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.wolf_scheduler.course.Activity#checkConflict(edu.ncsu.csc216.wolf_scheduler.course.Activity)}.
	 */
	@Test
	public void testOneDayConflict() {
		Activity a1 = new Course("MA242", "Calculus 3", "001", 4, "lkn", "MWF", 935, 1025);
		Activity a2 = new Event("Cardio", "F", 900, 945, 1, "Stairmaster session.");
		try {
			a1.checkConflict(a2);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("MWF 9:35AM-10:25AM", a1.getMeetingString());
			assertEquals("F 9:00AM-9:45AM (every 1 weeks)", a2.getMeetingString());
		}
		// Same values but with the parameter and 'this' switched to make sure that the method is commutative.
		try {
			a2.checkConflict(a1);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("MWF 9:35AM-10:25AM", a1.getMeetingString());
			assertEquals("F 9:00AM-9:45AM (every 1 weeks)", a2.getMeetingString());
		}
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.wolf_scheduler.course.Activity#checkConflict(edu.ncsu.csc216.wolf_scheduler.course.Activity)}.
	 */
	@Test
	public void testSameActivityConflict() {
		Activity a1 = new Course("E101", "Intro to Engineering", "001", 4, "jmadams", "W", 1230, 1420);
		Activity a2 = new Course("E101", "Intro to Engineering", "001", 4, "jmadams", "W", 1230, 1420);
		try {
			a1.checkConflict(a2);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("W 12:30PM-2:20PM", a1.getMeetingString());
			assertEquals("W 12:30PM-2:20PM", a2.getMeetingString());
		}
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.wolf_scheduler.course.Activity#checkConflict(edu.ncsu.csc216.wolf_scheduler.course.Activity)}.
	 */
	@Test
	public void testNoGapConflict() {
		Activity a1 = new Event("Lifting", "MWFS", 900, 1030, 1, "Upper/Lower Split");
		Activity a2 = new Event("Piano Practice", "FU", 1030, 1130, 1, "Practice session at Price Music Center");
		try {
			a1.checkConflict(a2);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("MWFS 9:00AM-10:30AM (every 1 weeks)", a1.getMeetingString());
			assertEquals("FU 10:30AM-11:30AM (every 1 weeks)", a2.getMeetingString());
		}
		// Same values but with the parameter and 'this' switched to make sure that the method is commutative.
		try {
			a2.checkConflict(a1);
			fail();
		} catch (ConflictException e) {
			// Check that the internal state didn't change during method call.
			assertEquals("MWFS 9:00AM-10:30AM (every 1 weeks)", a1.getMeetingString());
			assertEquals("FU 10:30AM-11:30AM (every 1 weeks)", a2.getMeetingString());
		}

	}

	/**
	 * Test method for {@link edu.ncsu.csc216.wolf_scheduler.course.Activity#checkConflict(edu.ncsu.csc216.wolf_scheduler.course.Activity)}.
	 */
	@Test
	public void testArranged() {
		Activity a1 = new Event("Midnight Run", "U", 0, 100, 4, "Monthly midnight run.");
		Activity a2 = new Course("PSY200", "Intro to Pyschology", "001", 3, "rponds", "A");
		try {
			a1.checkConflict(a2);
			assertEquals("Incorrect meeting string for this Activity.", "U 12:00AM-1:00AM (every 4 weeks)", a1.getMeetingString());
			assertEquals("Incorrect meeting string for possibleConflictingActivity.", "Arranged", a2.getMeetingString());
		} catch (ConflictException e) {
			fail("A ConflictException was thrown when one Arranged activity was compared to one in-person activity.");
		}
	}

}
