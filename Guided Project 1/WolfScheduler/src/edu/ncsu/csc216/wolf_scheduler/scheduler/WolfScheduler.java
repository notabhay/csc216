package edu.ncsu.csc216.wolf_scheduler.scheduler;

import java.util.*;
import java.io.*;

import edu.ncsu.csc216.wolf_scheduler.course.Course;
import edu.ncsu.csc216.wolf_scheduler.io.CourseRecordIO;

/**
 * WolfScheduler handles requests provided by the user
 *
 * @author Abhay Sriwastawa
 */
public class WolfScheduler {

    private ArrayList<Course> catalog;
    private ArrayList<Course> schedule;
    private String title;

    /**
     * Sets Catalog, Schedule and Title and Checks for FileNotFoundExceptions
     *
     * @param fileName fileName
     */
	public WolfScheduler(String fileName) {
        catalog = new ArrayList<Course>();
        schedule = new ArrayList<Course>();
        title = "My Schedule";
        try  {
			catalog = CourseRecordIO.readCourseRecords(fileName);
		}
		catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Cannot find file.");
		}
	}

    /**
     * Gets the Course from Catalog matching the given name and section
     *
     * @param name name
     * @param section section
     * @return course
     */
    public Course getCourseFromCatalog(String name, String section) {
        Course x = null;
        for (int i = 0; i < catalog.size(); i++) {
            x = catalog.get(i);
            if (x.getName().equals(name) && x.getSection().equals(section)) {
                return x;
            }
        }
        return null;
    }

    /**
     * Checks for and Adds Courses to Schedule from Catalog matching the given name and section
     *
     * @param name name
     * @param section section
     * @return true or false for whether or not course already exists in schedule
     */
    public boolean addCourse(String name, String section) {
		Course x = getCourseFromCatalog(name, section);
		Course y = null;
		if (x == null) {
			return false;
		}
		for (int i = 0; i < schedule.size(); i++) {
			y = schedule.get(i);
			if (y.getName().equals(name)) {
				throw new IllegalArgumentException("You are already enrolled in " + name);
			}
		}
		schedule.add(x);
		return true;
	}

    /**
     * Removes courses from schedule matching the given name and section
     *
     * @param name name
     * @param section section
     * @return true or false for whether or not course has already been removed from schedule
     */
    public boolean removeCourse(String name, String section) {
		Course x = null;
		for (int i = 0; i < schedule.size(); i++) {
			x = schedule.get(i);
			if (x.getName().equals(name) && x.getSection().equals(section)) {
				schedule.remove(i);
				return true;
			}
		}
		return false;
	}

    /**
     * Resets Schedule
     */
    public void resetSchedule() {
        schedule = new ArrayList<Course>();
    }

    /**
     * Gets Course Catalog
     *
     * @return Returns Course Catalog
     */
	public String[][] getCourseCatalog() {
        Course x = null;
        String[][] y = new String[catalog.size()][3];
        for (int i = 0; i < catalog.size(); i++) {
            x = catalog.get(i);
            y[i][0] = x.getName();
            y[i][1] = x.getSection();
            y[i][2] = x.getTitle();
        }
        return y;
    }

    /**
     * Gets Scheduled Courses from schedule
     *
     * @return Returns Scheduled Courses
     */
    public String[][] getScheduledCourses() {
    	String[][] x = new String[schedule.size()][3];
    	Course y = null;
    	for(int i = 0; i < schedule.size(); i++) {
    		y = schedule.get(i);
    		x[i][0] = y.getName();
    		x[i][1] = y.getSection();
    		x[i][2] = y.getTitle();
    		}
    	return x;
    }

    /**
     * Gets Full Scheduled Courses from schedule
     *
     * @return Returns Full Scheduled Courses
     */
    public String[][] getFullScheduledCourses() {
		String[][] x = new String[schedule.size()][6];
		Course y = null;
		for (int i = 0; i < schedule.size(); i++) {
			y = schedule.get(i);
			x[i][0] = y.getName();
			x[i][1] = y.getSection();
			x[i][2] = y.getTitle();
			x[i][3] = Integer.toString(y.getCredits());
			x[i][4] = y.getInstructorId();
			x[i][5] = y.getMeetingString();
		}
		return x;
	}

    /**
     * Gets Title
     *
     * @return Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets Title provided by the user
     *
     * @param title title
     */
    public void setTitle(String title) {
		if(title == null) {
			throw new IllegalArgumentException("Title cannot be null");
		}
		this.title = title;

	}

    /**
     * Exports Schedule with writeCourseRecords method to a fileName provided by the user
     *
     * @param fileName fileName
     */
    public void exportSchedule(String fileName) {
		try {
			CourseRecordIO.writeCourseRecords(fileName, schedule);
		}
		catch (IOException e) {
			throw new IllegalArgumentException("The file cannot be saved.");
		}
	}

}
