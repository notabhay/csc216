/**
 *
 */
package edu.ncsu.csc216.wolf_scheduler.course;

/**
 * Course Class Constructs and Edits Course objects
 *
 * @author Abhay
 *
 */
public class Course {

	/** Course's name. */
	private String name;
	/** Course's title. */
	private String title;
	/** Course's section. */
	private String section;
	/** Course's credit hours */
	private int credits;
	/** Course's instructor */
	private String instructorId;
	/** Course's meeting days */
	private String meetingDays;
	/** Course's starting time */
	private int startTime;
	/** Course's ending time */
	private int endTime;

	/**
	 * Constructs a Course object with values for all fields.
	 * @param name name of Course
	 * @param title title of Course
	 * @param section section of Course
	 * @param credits credit hours for Course
	 * @param instructorId instructor's unity id
	 * @param meetingDays meeting days for Course as series of chars
	 * @param startTime start time for Course
	 * @param endTime end time for Course
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays,
	        int startTime, int endTime) {
	    setName(name);
	    setTitle(title);
	    setSection(section);
	    setCredits(credits);
	    setInstructorId(instructorId);
	    setMeetingDays(meetingDays);
        setCourseTime(startTime, endTime);
	}

	/**
	 * Creates a Course with the given name, title, section, credits, instructorId, and meetingDays for
	 * courses that are arranged.
	 * @param name name of Course
	 * @param title title of Course
	 * @param section section of Course
	 * @param credits credit hours for Course
	 * @param instructorId instructor's unity id
	 * @param meetingDays meeting days for Course as series of chars
	 */
	public Course(String name, String title, String section, int credits, String instructorId, String meetingDays) {
	    this(name, title, section, credits, instructorId, meetingDays, 0, 0);
	}

	/**
     * Returns the Course's name
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the Course's name.  If the name is null, has a length less than 4 or
	 * greater than 6, an IllegalArgumentException is thrown.
	 * @param name the name to set
	 * @throws IllegalArgumentException if name is null or length is less than 4 or
	 * greater than 6
	 */
	private void setName(String name) {
	    if (name == null) {
	        throw new IllegalArgumentException();
	    }
	    if (name.length() < 4 || name.length() > 6) {
	        throw new IllegalArgumentException();
	    }
	    this.name = name;
	}

	/**
     * Returns the Course's title
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

    /**
     * Sets the Course's title.  If the title is null, has a length of 0, an
     * IllegalArgumentException is thrown.
     * @param title the title to set
     * @throws IllegalArgumentException if title is null or has a length of 0
     */
    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException();
        }
        if (title.length() == 0) {
            throw new IllegalArgumentException();
        }
        this.title = title;
    }

	/**
     * Returns the Course's section
	 * @return the section
	 */
	public String getSection() {
		return section;
	}

    /**
     * Sets the Course's section.  If the section has a length of 3, an
     * IllegalArgumentException is thrown.
     * @param section the section to set
     * @throws IllegalArgumentException if the section has a length of 3
     */
    public void setSection(String section) {
        if (section == null) {
            throw new IllegalArgumentException();
        }
        if (section.length() != 3) {
            throw new IllegalArgumentException();
        }
        this.section = section;
    }

	/**
     * Returns the Course's credits
	 * @return the credits
	 */
	public int getCredits() {
		return credits;
	}

    /**
     * Sets the Course's credits.  If the credit hours are less than 1 or greater than 5 or not a
     * number, an IllegalArgumentException is thrown.
     * @param credits the credits to set
     * @throws IllegalArgumentException if the credits has a length of 3
     */
    public void setCredits(int credits) {
        if (credits < 1 || credits > 5) {
	        throw new IllegalArgumentException();
	    }
        this.credits = credits;
    }

	/**
     * Returns the Course's instructorId
	 * @return the instructorId
	 */
	public String getInstructorId() {
		return instructorId;
	}

    /**
     * Sets the Course's instructorId.  If the instructorId is null an
     * IllegalArgumentException is thrown.
     * @param instructorId the instructorId to set
     * @throws IllegalArgumentException if instructorId is null
     */
    public void setInstructorId(String instructorId) {
        if (instructorId == null) {
            throw new IllegalArgumentException();
        }
        if (instructorId.length() == 0) {
            throw new IllegalArgumentException();
        }
        this.instructorId = instructorId;
    }

	/**
     * Returns the Course's meetingDays
	 * @return the meetingDays
	 */
	public String getMeetingDays() {
		return meetingDays;
	}

    /**
     * Sets the Course's meetingDays.  If meeting days consist of any characters other than
     * ‘M’, ‘T’, ‘W’, ‘H’, ‘F’, or ‘A’ or if ‘A’ is in the meeting days list, it must be
     * the only character or else an IllegalArgumentException is thrown.
     * @param meetingDays the meetingDays to set
     * @throws IllegalArgumentException if meetingDays is null
     */
    public void setMeetingDays(String meetingDays) {
        if (meetingDays == null) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.length() == 0) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.contains("B") || meetingDays.contains("C")
                || meetingDays.contains("D") || meetingDays.contains("E")
                || meetingDays.contains("G") || meetingDays.contains("I")
                || meetingDays.contains("J") || meetingDays.contains("K")
                || meetingDays.contains("L") || meetingDays.contains("N")
                || meetingDays.contains("O") || meetingDays.contains("P")
                || meetingDays.contains("Q") || meetingDays.contains("R")
                || meetingDays.contains("S") || meetingDays.contains("U")
                || meetingDays.contains("V") || meetingDays.contains("X")
                || meetingDays.contains("Y") || meetingDays.contains("Z")) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.contains("M") && meetingDays.contains("A") || meetingDays.contains("T") && meetingDays.contains("A")
            || meetingDays.contains("W") && meetingDays.contains("A") || meetingDays.contains("H") && meetingDays.contains("A")
            || meetingDays.contains("F") && meetingDays.contains("A")) {
            throw new IllegalArgumentException();
        }
        this.meetingDays = meetingDays;
    }

	/**
     * Returns the Course's startTime
	 * @return the startTime
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
     * Returns the Course's endTime
	 * @return the endTime
	 */
	public int getEndTime() {
		return endTime;
	}

    /**
     * Sets the Course's start and end time.
     * If startTime or endTime is not between 0000 and 2359 IllegalArgumentException is thrown
     * If the end time is less than the start time IllegalArgumentException is thrown
     * If a start time and/or end time is listed when meeting days is ‘A’ an
     * IllegalArgumentException is thrown
     * @param startTime the startTime to set
     * @param endTime the endTime to set
     * @throws IllegalArgumentException if startTime or endTime is null
     */
    public void setCourseTime(int startTime, int endTime) {
        int hours = startTime / 100;
        int mins = startTime % 100;
        if (hours < 0 || hours > 23 || mins < 0 || mins > 59) {
            throw new IllegalArgumentException();
        }
        hours = endTime / 100;
        mins = endTime % 100;
        if (hours < 0 || hours > 23 || mins < 0 || mins > 59) {
            throw new IllegalArgumentException();
        }
        if (startTime > endTime) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.contains("A") && startTime != 0) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.contains("A") && endTime != 0) {
            throw new IllegalArgumentException();
        }
        this.startTime = startTime;
        this.endTime = endTime;
    }
    /**
     * Returns Meeting String with the correct Format
     * @return Formatted Meeting String
     */
    public String getMeetingString() {
        if (meetingDays.contains("A")) {
            return "Arranged";
        } else {
            int startTimeHours = startTime / 100;
            int startTimeMins = startTime % 100;
            String startTimeAmPm = "";
            if (startTimeHours < 12) {
                startTimeAmPm = "AM";
                if (startTimeHours == 0) {
                    startTimeHours = 12;
                }
            } else {
                startTimeAmPm = "PM";
                if (startTimeHours != 12) {
                    startTimeHours = startTimeHours - 12;
                }
            }
            int endTimeHours = endTime / 100;
            int endTimeMins = endTime % 100;
            String endTimeAmPm = "";
            if (endTimeHours < 12) {
                endTimeAmPm = "AM";
                if (endTimeHours == 0) {
                    endTimeHours = 12;
                }
            } else {
                endTimeAmPm = "PM";
                if (endTimeHours != 12) {
                    endTimeHours = endTimeHours - 12;
                }
            }
            if (startTimeMins < 10 && endTimeMins < 10) {
                return meetingDays + " " + startTimeHours + ":" + "0" + startTimeMins +
                    startTimeAmPm + "-" + endTimeHours + ":" + "0" + endTimeMins + endTimeAmPm;
            } else if (startTimeMins < 10) {
                return meetingDays + " " + startTimeHours + ":" + "0" + startTimeMins +
                    startTimeAmPm + "-" + endTimeHours + ":" + endTimeMins + endTimeAmPm;
            } else if (endTimeMins < 10) {
                return meetingDays + " " + startTimeHours + ":" + startTimeMins +
                    startTimeAmPm + "-" + endTimeHours + ":" + "0" + endTimeMins + endTimeAmPm;
            }
            return meetingDays + " " + startTimeHours + ":" + startTimeMins +
                startTimeAmPm + "-" + endTimeHours + ":" + endTimeMins + endTimeAmPm;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + credits;
		result = prime * result + endTime;
		result = prime * result + ((instructorId == null) ? 0 : instructorId.hashCode());
		result = prime * result + ((meetingDays == null) ? 0 : meetingDays.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		result = prime * result + startTime;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (credits != other.credits)
			return false;
		if (endTime != other.endTime)
			return false;
		if (instructorId == null) {
			if (other.instructorId != null)
				return false;
		} else if (!instructorId.equals(other.instructorId))
			return false;
		if (meetingDays == null) {
			if (other.meetingDays != null)
				return false;
		} else if (!meetingDays.equals(other.meetingDays))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		if (startTime != other.startTime)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/**
	 * Returns a comma separated value String of all Course fields.
	 * @return String representation of Course
	 */
	@Override
	public String toString() {
	    if (meetingDays.equals("A")) {
	        return name + "," + title + "," + section + "," + credits + "," + instructorId + "," + meetingDays;
	    }
	    return name + "," + title + "," + section + "," + credits + "," + instructorId + "," + meetingDays + "," + startTime + "," + endTime;
	}
}
