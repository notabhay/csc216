package edu.ncsu.csc216.wolf_scheduler.course;

/**
 * Conflict throws the checked ConflictException if the possibly conflicting activity (parameter) conflicts with the current activity (this).
 *
 * @author Abhay
 */
public interface Conflict {

    /**
     * This method throws the checked ConflictException if the possibly conflicting activity (parameter) conflicts with the current activity (this).
     *
     * @param possibleConflictingActivity possibleConflictingActivity
     * @throws ConflictException throws ConflictException if the possibly conflicting activity (parameter) conflicts with the current activity (this)
     */
    void checkConflict(Activity possibleConflictingActivity) throws ConflictException;

}
