package edu.ncsu.csc216.wolf_scheduler.course;

/**
 * Event Class Constructs and Edits Event objects
 *
 * @author Abhay
 *
 */
public class Event extends Activity {

    /** Int that describes how many weeks between events  */
    private int weeklyRepeat;

    /** String containing the event details */
    private String eventDetails;

    /**
     * Sets the fields to the given values or throws an IllegalArgumentException
     * if provided an invalid value.
     *
     * @param title title
     * @param meetingDays meetingDays
     * @param startTime startTime
     * @param endTime endTime
     * @param weeklyRepeat weeklyRepeat
     * @param eventDetails eventDetails
     */
    public Event(String title, String meetingDays, int startTime, int endTime, int weeklyRepeat, String eventDetails) {
        super(title, meetingDays, startTime, endTime);
        setWeeklyRepeat(weeklyRepeat);
        setEventDetails(eventDetails);
    }

    /**
    * Sets weekly repeat and throws an IllegalArgumentException if the parameter is less than one or greater than 4.
    *
    * @param weeklyRepeat weeklyRepeat
    */
    public void setWeeklyRepeat(int weeklyRepeat) {
        if (weeklyRepeat < 1 || weeklyRepeat > 4) {
            throw new IllegalArgumentException("Invalid weekly repeat");
        }
        this.weeklyRepeat = weeklyRepeat;
    }

    /**
    * Gets Weekly Repeat
    *
    * @return weeklyRepeat
    */
    public int getWeeklyRepeat() {
        return weeklyRepeat;
    }

    /**
    * Sets event deatials and throws an IllegalArgumentException if the parameter is null.
    *
    * @param eventDetails eventDetails
    */
    public void setEventDetails(String eventDetails) {
        if (eventDetails == null) {
            throw new IllegalArgumentException("Invalid event details");
        }
        this.eventDetails = eventDetails;
    }

    /**
     * Gets Event Details
     *
     * @return eventDetails
     */
	public String getEventDetails() {
		return eventDetails;
	}

    /**
     * Checks if the given Activity is a duplicate of the Event by title.
     *
     * @param activity activity
     * @return Returns true if the given Activity is a duplicate of the Event by title.
     */
    public boolean isDuplicate(Activity activity) {
        if(activity instanceof Event) {
            Event event = (Event) activity;
            return this.getTitle().equals(event.getTitle());
        }
        return false;
    }

    @Override
    public void setMeetingDays(String meetingDays) {
        if (meetingDays == null) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.length() == 0) {
            throw new IllegalArgumentException();
        }
        if (meetingDays.contains("A") || meetingDays.contains("B") || meetingDays.contains("C")
        || meetingDays.contains("D") || meetingDays.contains("E")
        || meetingDays.contains("G") || meetingDays.contains("I")
        || meetingDays.contains("J") || meetingDays.contains("K")
        || meetingDays.contains("L") || meetingDays.contains("N")
        || meetingDays.contains("O") || meetingDays.contains("P")
        || meetingDays.contains("Q") || meetingDays.contains("R")
        || meetingDays.contains("V") || meetingDays.contains("X")
        || meetingDays.contains("Y") || meetingDays.contains("Z")) {
            throw new IllegalArgumentException();
        }
        super.setMeetingDays(meetingDays);
    }

    @Override
    public String getMeetingString() {
        return super.getMeetingString() + " (every " + getWeeklyRepeat() + " weeks)";
    }

    @Override
    public String toString() {
        return getTitle() + "," + getMeetingDays() + "," + getStartTime() + "," + getEndTime() + "," + getWeeklyRepeat() + "," + getEventDetails();
    }

	@Override
	public String[] getShortDisplayArray() {
        String[] x = {"", "", getTitle(), getMeetingString()};
		return x;
	}

	@Override
	public String[] getLongDisplayArray() {
        String[] x = {"", "", getTitle(), "", "", getMeetingString(), getEventDetails()};
		return x;
	}
}
