package edu.ncsu.csc216.wolf_scheduler.io;

import java.io.*;
import java.util.*;

import edu.ncsu.csc216.wolf_scheduler.course.Activity;

/**
 * ActivityRecordIO handles list of courses in Activity
 *
 * @author Abhay
 */
public class ActivityRecordIO {

	/**
	 * ActivityRecordIO Constructor
	 */
	public ActivityRecordIO(){
		//Empty Constructor For Testing
	}

    /**
     * Writes the given list of Courses
     * @param fileName file to save to
     * @param courses list of course to save
     * @throws IOException if the file cannot be written
     */
    public static void writeActivityRecords(String fileName, ArrayList<Activity> courses) throws IOException {
        PrintStream fileWriter = new PrintStream(new File(fileName));
        for (Activity c : courses) {
            fileWriter.println(c.toString());
        }
        fileWriter.close();
    }
}
