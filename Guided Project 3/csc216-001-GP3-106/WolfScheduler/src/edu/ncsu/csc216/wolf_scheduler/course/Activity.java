package edu.ncsu.csc216.wolf_scheduler.course;

/**
 *  Activity Class Constructs and Edits Activity objects
 *
 * @author Abhay
 */
public abstract class Activity implements Conflict {

    /** Constant containing the upper military time, which is a value of 2400 */
    private static final int UPPER_TIME = 2400;
    /** Constant containing the upper value for an hour, which is a value of 60 */
    private static final int UPPER_HOUR = 60;
	/** Course's title. */
	private String title;
	/** Course's meeting days */
	private String meetingDays;
	/** Course's starting time */
	private int startTime;
	/** Course's ending time */
	private int endTime;

    /**
     * Returns an array of length 4 containing the Course name, section, title, and meeting days string.
     *
     * @return Returns an array of length 4
     */
    public abstract String[] getShortDisplayArray();

    /**
     * Returns an array of length 7 containing the Course name, section, title, credits, instructorId, meeting days string, empty
     * string (for a field that Event will have that Course does not).
     *
     * @return Returns an array of length 7
     */
    public abstract String[] getLongDisplayArray();

    /**
     * Returns true if the given Activity is a duplicate of the Event by title.
     *
     * @param activity activity
     * @return Returns true if the given Activity is a duplicate of the Event by title.
     */
    public abstract boolean isDuplicate(Activity activity);

    /**
	 * Constructs a Course object with values for given fields.
	 * @param title title of Course
	 * @param meetingDays meeting days for Course as series of chars
	 * @param startTime start time for Course
	 * @param endTime end time for Course
	 */
	public Activity(String title, String meetingDays, int startTime, int endTime) {
	    setTitle(title);
	    setMeetingDays(meetingDays);
        setActivityTime(startTime, endTime);
	}

	/**
	 * Returns the Course's title
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the Course's title.  If the title is null, has a length of 0, an
	 * IllegalArgumentException is thrown.
	 * @param title the title to set
	 * @throws IllegalArgumentException if title is null or has a length of 0
	 */
	public void setTitle(String title) {
	    if (title == null) {
	        throw new IllegalArgumentException();
	    }
	    if (title.length() == 0) {
	        throw new IllegalArgumentException();
	    }
	    this.title = title;
	}

	/**
	 * Returns the Course's meetingDays
	 * @return the meetingDays
	 */
	public String getMeetingDays() {
		return meetingDays;
	}

	/**
	 * Sets the Course's meetingDays.  If meeting days consist of any characters other than
	 * ‘M’, ‘T’, ‘W’, ‘H’, ‘F’, or ‘A’ or if ‘A’ is in the meeting days list, it must be
	 * the only character or else an IllegalArgumentException is thrown.
	 * @param meetingDays the meetingDays to set
	 * @throws IllegalArgumentException if meetingDays is null
	 */
	public void setMeetingDays(String meetingDays) {
	    if ((meetingDays == null) || (meetingDays.length() == 0)) {
	        throw new IllegalArgumentException();
	    }
	    this.meetingDays = meetingDays;
	}

	/**
	 * Returns the Course's startTime
	 * @return the startTime
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
	 * Returns the Course's endTime
	 * @return the endTime
	 */
	public int getEndTime() {
		return endTime;
	}

	/**
	 * Sets the Course's start and end time.
	 * If startTime or endTime is not between 0000 and 2359 IllegalArgumentException is thrown
	 * If the end time is less than the start time IllegalArgumentException is thrown
	 * If a start time and/or end time is listed when meeting days is ‘A’ an
	 * IllegalArgumentException is thrown
	 * @param startTime the startTime to set
	 * @param endTime the endTime to set
	 * @throws IllegalArgumentException if startTime or endTime is null
	 */
	public void setActivityTime(int startTime, int endTime) {
	    int hours = startTime / 100;
	    int mins = startTime % 100;
	    if (hours < 0 || hours >= UPPER_TIME / 100 || mins < 0 || mins >= UPPER_HOUR) {
	        throw new IllegalArgumentException();
	    }
	    hours = endTime / 100;
	    mins = endTime % 100;
	    if (hours < 0 || hours >= UPPER_TIME / 100 || mins < 0 || mins >= UPPER_HOUR) {
	        throw new IllegalArgumentException();
	    }
	    if (startTime > endTime) {
	        throw new IllegalArgumentException();
	    }
	    if ((meetingDays.contains("A") && startTime != 0) || (meetingDays.contains("A") && endTime != 0)) {
	        throw new IllegalArgumentException();
	    }
	    this.startTime = startTime;
	    this.endTime = endTime;
	}

	/**
	 * Returns Meeting String with the correct Format
	 * @return Formatted Meeting String
	 */
	public String getMeetingString() {
	    if (meetingDays.contains("A")) {
	        return "Arranged";
	    } else {
	        int startTimeHours = startTime / 100;
	        int startTimeMins = startTime % 100;
	        String startTimeAmPm = "";
	        if (startTimeHours < 12) {
	            startTimeAmPm = "AM";
	            if (startTimeHours == 0) {
	                startTimeHours = 12;
	            }
	        } else {
	            startTimeAmPm = "PM";
	            if (startTimeHours != 12) {
	                startTimeHours = startTimeHours - 12;
	            }
	        }
	        int endTimeHours = endTime / 100;
	        int endTimeMins = endTime % 100;
	        String endTimeAmPm = "";
	        if (endTimeHours < 12) {
	            endTimeAmPm = "AM";
	            if (endTimeHours == 0) {
	                endTimeHours = 12;
	            }
	        } else {
	            endTimeAmPm = "PM";
	            if (endTimeHours != 12) {
	                endTimeHours = endTimeHours - 12;
	            }
	        }
	        if (startTimeMins < 10 && endTimeMins < 10) {
	            return meetingDays + " " + startTimeHours + ":" + "0" + startTimeMins +
	                startTimeAmPm + "-" + endTimeHours + ":" + "0" + endTimeMins + endTimeAmPm;
	        } else if (startTimeMins < 10) {
	            return meetingDays + " " + startTimeHours + ":" + "0" + startTimeMins +
	                startTimeAmPm + "-" + endTimeHours + ":" + endTimeMins + endTimeAmPm;
	        } else if (endTimeMins < 10) {
	            return meetingDays + " " + startTimeHours + ":" + startTimeMins +
	                startTimeAmPm + "-" + endTimeHours + ":" + "0" + endTimeMins + endTimeAmPm;
	        }
	        return meetingDays + " " + startTimeHours + ":" + startTimeMins +
	            startTimeAmPm + "-" + endTimeHours + ":" + endTimeMins + endTimeAmPm;
	    }
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + endTime;
        result = prime * result + ((meetingDays == null) ? 0 : meetingDays.hashCode());
        result = prime * result + startTime;
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        Activity other = (Activity) obj;
        if (endTime != other.endTime){
            return false;
        }
        if (!meetingDays.equals(other.meetingDays)) {
            return false;
        }
        if (startTime != other.startTime){
            return false;
        }
        if (!title.equals(other.title)){
            return false;
        }
        return true;
    }

    @Override
	public void checkConflict(Activity checkThisActivity) throws ConflictException {
        if (this.getMeetingDays().contains("A") || checkThisActivity.getMeetingDays().contains("A")) {
            return;
        }
		boolean x = false;
		for (int i = 0; i < this.getMeetingDays().length(); i++) {
			String y = "";
            y += this.getMeetingDays().charAt(i);
			if (checkThisActivity.getMeetingDays().contains(y)) {
				x = true;
			}
		}
		if (x) {
			int a = this.getStartTime();
			int b = this.getEndTime();
			int c = checkThisActivity.getStartTime();
			int d = checkThisActivity.getEndTime();
			if (a <= c && c <= b) {
				throw new ConflictException();
			} else if (c <= a && a <= d) {
				throw new ConflictException();
			}
		}
	}
}
