package edu.ncsu.csc216.wolf_scheduler.scheduler;

import java.util.*;
import java.io.*;

import edu.ncsu.csc216.wolf_scheduler.course.Activity;
import edu.ncsu.csc216.wolf_scheduler.course.ConflictException;
import edu.ncsu.csc216.wolf_scheduler.course.Course;
import edu.ncsu.csc216.wolf_scheduler.course.Event;
import edu.ncsu.csc216.wolf_scheduler.io.ActivityRecordIO;
import edu.ncsu.csc216.wolf_scheduler.io.CourseRecordIO;

/**
 * WolfScheduler handles requests provided by the user
 *
 * @author Abhay Sriwastawa
 */
public class WolfScheduler {

    /** ArrayList Course Catalog */
    private ArrayList<Course> catalog;
    /** ArrayList Activity Schedule */
    private ArrayList<Activity> schedule;
    /** String title */
    private String title;

    /**
     * Sets Catalog, Schedule and Title and Checks for FileNotFoundExceptions
     *
     * @param fileName fileName
     */
	public WolfScheduler(String fileName) {
        catalog = new ArrayList<Course>();
        schedule = new ArrayList<Activity>();
        title = "My Schedule";
        try  {
			catalog = CourseRecordIO.readCourseRecords(fileName);
		}
		catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Cannot find file.");
		}
	}

    /**
     * Gets the Course from Catalog matching the given name and section
     *
     * @param name name
     * @param section section
     * @return course
     */
    public Course getCourseFromCatalog(String name, String section) {
        Course x = null;
        for (int i = 0; i < catalog.size(); i++) {
            x = catalog.get(i);
            if (x.getName().equals(name) && x.getSection().equals(section)) {
                return x;
            }
        }
        return null;
    }

    /**
     * Checks for and Adds Courses to Schedule from Catalog matching the given name and section
     *
     * @param name name
     * @param section section
     * @return true or false for whether or not course already exists in schedule
     */
    public boolean addCourse(String name, String section) {
     	Course x = getCourseFromCatalog(name, section);
     	if(x == null) {
     		return false;
     	}
     	Activity y = null;
     	for(int i = 0; i < schedule.size(); i++) {
     		y = schedule.get(i);
     		if(x.isDuplicate(y)) {
     			throw new IllegalArgumentException("You are already enrolled in " + name);
     		}
            try {
                y.checkConflict(x);
            } catch (ConflictException e) {
                throw new IllegalArgumentException("The course cannot be added due to a conflict.");
            }
     	}
     	schedule.add(x);
     	return true;
    }

    /**
     * Returns true if the Activity is removed from the schedule and false otherwise.
     * @param idx index of the Activity you want to remove from the schedule.
     * @return true or false for whether or not course has already been removed from schedule
     */
    public boolean removeActivity(int idx) {
        if (idx < 0 || idx > schedule.size() - 1) {
            return false;
        }
        schedule.remove(idx);
        return true;
	}

    /**
     * Resets Schedule
     */
    public void resetSchedule() {
        schedule = new ArrayList<Activity>();
    }

    /**
     * Gets Course Catalog
     *
     * @return Returns Course Catalog
     */
	public String[][] getCourseCatalog() {
        Course x = null;
        String[][] y = new String[catalog.size()][4];
        for (int i = 0; i < catalog.size(); i++) {
            x = catalog.get(i);
            String[] a = x.getShortDisplayArray();
            for (int j = 0; j < 4; j++) {
                y[i][j] = a[j];
            }
        }
        return y;
    }

    /**
     * Gets Scheduled Courses from schedule
     *
     * @return Returns Scheduled Courses
     */
    public String[][] getScheduledActivities() {
        Activity x = null;
		String[][] y = new String[schedule.size()][4];
        for(int i = 0; i < schedule.size(); i++) {
            x = schedule.get(i);
            String[] a = x.getShortDisplayArray();
            for (int j = 0; j < 4; j++) {
                y[i][j] = a[j];
            }
    	}
        return y;
    }

    /**
     * Gets Full Scheduled Courses from schedule
     *
     * @return Returns Full Scheduled Courses
     */
    public String[][] getFullScheduledActivities() {
        Activity x = null;
		String[][] y = new String[schedule.size()][7];
        for(int i = 0; i < schedule.size(); i++) {
            x = schedule.get(i);
            String[] a = x.getLongDisplayArray();
            for (int j = 0; j < 7; j++) {
                y[i][j] = a[j];
            }
    	}
        return y;
	}

    /**
     * Gets Title
     *
     * @return Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets Title provided by the user
     *
     * @param title title
     */
    public void setTitle(String title) {
		if(title == null) {
			throw new IllegalArgumentException("Title cannot be null");
		}
		this.title = title;

	}

    /**
     * Exports Schedule with writeCourseRecords method to a fileName provided by the user
     *
     * @param fileName fileName
     */
    public void exportSchedule(String fileName) {
		try {
			ActivityRecordIO.writeActivityRecords(fileName, schedule);
		}
		catch (IOException e) {
			throw new IllegalArgumentException("The file cannot be saved.");
		}
	}

    /**
     * throws an IllegalArgumentException if the Event cannot be constructed or if it is a duplicate of an Event already in the schedule. Otherwise, the event is added to the end of the schedule.
     *
     * @param eventTitle eventTitle
     * @param eventMeetingDays eventMeetingDays
     * @param eventStartTime eventStartTime
     * @param eventEndTime eventEndTime
     * @param eventWeeklyRepeat eventWeeklyRepeat
     * @param eventDetails eventDetails
     */
	public void addEvent(String eventTitle, String eventMeetingDays, int eventStartTime, int eventEndTime, int eventWeeklyRepeat, String eventDetails) {
        Event x = null;
        try {
            x = new Event(eventTitle, eventMeetingDays, eventStartTime, eventEndTime, eventWeeklyRepeat, eventDetails);
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException("Event cannot be constructed");
        }
        Activity y = null;
        for(int i = 0; i < schedule.size(); i++) {
     		y = schedule.get(i);
     		if(x.isDuplicate(y)) {
                throw new IllegalArgumentException("You have already created an event called " + x.getTitle());
     		}
            try {
				y.checkConflict(x);
			} catch (ConflictException e) {
				throw new IllegalArgumentException("The event cannot be added due to a conflict.");
			}
     	}
        schedule.add(x);
    }
}
