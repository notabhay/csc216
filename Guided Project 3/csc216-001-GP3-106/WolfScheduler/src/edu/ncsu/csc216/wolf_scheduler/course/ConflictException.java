package edu.ncsu.csc216.wolf_scheduler.course;

/**
 * The ConflictException is a checked exception. The default message is “Schedule conflict.”
 *
 * @author Abhay
 */
public class ConflictException extends Exception {

	/** ID used for serialization. */
	private static final long serialVersionUID = 1L;

    /**
     * Creates an exception with the given message.
     *
     * @param message message
     */
    public ConflictException(String message){
        super(message);
    }

    /**
     * Creates an exception with the default message.
     */
    public ConflictException(){
        this("Schedule conflict.");
    }
}
