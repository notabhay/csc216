package edu.ncsu.csc216.ticket_manager.model.manager;

import java.util.List;

import edu.ncsu.csc216.ticket_manager.model.command.Command;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Category;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Priority;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType;

/**
 * TicketList
 * 
 * @author Abhay
 */
public class TicketList {
    /**
     * [TicketList description]
     */
    public TicketList() {
    	//Empty Constructor for now
    }

    /**
     * [addTicket description]
     *
     * @param  ticketType [description]
     * @param  str1       [description]
     * @param  str2       [description]
     * @param  cat        [description]
     * @param  prio       [description]
     * @param  str3       [description]
     * @return            [description]
     */
    public int addTicket(TicketType ticketType, String str1, String str2, Category cat, Priority prio, String str3) {
        return 0;
    }

    /**
     * [addTickets description]
     *
     * @param list [description]
     */
    public void addTickets(List<Ticket> list) {
    	//Empty method for now
    }

    /**
     * [getTickets description]
     *
     * @return [description]
     */
    public List<Ticket> getTickets() {
        return null;
    }

    /**
     * [getTicketsByType description]
     *
     * @param  ticketType [description]
     * @return            [description]
     */
    public List<Ticket> getTicketsByType(TicketType ticketType) {
        return null;
    }

    /**
     * [getTicketById description]
     *
     * @param  id [description]
     * @return    [description]
     */
    public Ticket getTicketById(int id) {
        return null;
    }

    /**
     * [executeCommand description]
     *
     * @param x       [description]
     * @param command [description]
     */
    public void executeCommand(int x, Command command) {
    	//Empty method for now
    }

    /**
     * [deleteTicketById description]
     *
     * @param id [description]
     */
    public void deleteTicketById(int id) {
    	//Empty method for now
    }
}
