package edu.ncsu.csc216.ticket_manager.model.ticket;

import java.util.ArrayList;

import edu.ncsu.csc216.ticket_manager.model.command.Command;

/**
 * Ticket
 *
 * @author Abhay
 */
public class Ticket {
    /** Constant */
    public static final String TT_REQUEST      = "Request";
    /** Constant */
    public static final String TT_INCIDENT     = "Incident";
    /** Constant */
    public static final String C_INQUIRY       = "Inquiry";
    /** Constant */
    public static final String C_SOFTWARE      = "Software";
    /** Constant */
    public static final String C_HARDWARE      = "Hardware";
    /** Constant */
    public static final String C_NETWORK       = "Network";
    /** Constant */
    public static final String C_DATABASE      = "Database";
    /** Constant */
    public static final String P_URGENT        = "Urgent";
    /** Constant */
    public static final String P_HIGH          = "High";
    /** Constant */
    public static final String P_MEDIUM        = "Medium";
    /** Constant */
    public static final String P_LOW           = "Low";
    /** Constant */
    public static final String NEW_NAME        = "New";
    /** Constant */
    public static final String WORKING_NAME    = "Working";
    /** Constant */
    public static final String FEEDBACK_NAME   = "Feedback";
    /** Constant */
    public static final String RESOLVED_NAME   = "Resolved";
    /** Constant */
    public static final String CLOSED_NAME     = "Closed";
    /** Constant */
    public static final String CANCELED_NAME   = "Canceled";

    /** Constant */
    private static int counter = 1;
    /** Constant */
    private int ticketId;
    /** Constant */
    private String subject;
    /** Constant */
    private String caller;
    /** Constant */
    private String owner;
    /** Constant */
    private ArrayList<String> notes;

    /**
     * [incrementCounter description]
     */
    public static void incrementCounter() {
        Ticket.counter = Ticket.counter + 1;
    }

    /**
     * [setCounter description]
     *
     * @param a [description]
     */
    public static void setCounter(int a) {
    	//Empty method for now

    }

    /**
     * [Ticket description]
     *
     * @param ticketType [description]
     * @param subject    [description]
     * @param caller     [description]
     * @param category   [description]
     * @param priority   [description]
     * @param notes      [description]
     */
    public Ticket(TicketType ticketType, String subject, String caller, Category category, Priority priority, String notes) {
        String str = "str";
        setCaller(str);
        setCancellationCode(str);
        setCategory(str);
        setOwner(str);
        setFeedbackCode(str);
        setPriority(str);
        setResolutionCode(str);
        setState(str);
        setSubject(str);
        setTicketType(str);
    }

    /**
     * [Ticket description]
     *
     * @param id         [description]
     * @param state      [description]
     * @param ticketType [description]
     * @param subject    [description]
     * @param caller     [description]
     * @param category   [description]
     * @param priority   [description]
     * @param owner      [description]
     * @param code       [description]
     * @param notes      [description]
     */
    public Ticket(int id, String state, String ticketType, String subject, String caller, String category, String priority, String owner, String code, ArrayList<String> notes) {
    	//Empty Constructor for now
    }

    /**
     * [getCaller description]
     *
     * @return [description]
     */
    public String getCaller() {
        return caller;
    }

    /**
     * [getCancellationCode description]
     *
     * @return [description]
     */
    public String getCancellationCode() {
        return null;
    }

    /**
     * [getCategory description]
     *
     * @return [description]
     */
    public String getCategory() {
        return null;
    }

    /**
     * [getFeedbackCode description]
     *
     * @return [description]
     */
    public String getFeedbackCode() {
        return null;
    }

    /**
     * [getNotes description]
     *
     * @return [description]
     */
    public String getNotes() {
        return notes.toString();
    }

    /**
     * [getOwner description]
     *
     * @return [description]
     */
    public String getOwner() {
        return owner;
    }

    /**
     * [getPriority description]
     *
     * @return [description]
     */
    public String getPriority() {
        return null;
    }

    /**
     * [getResolutionCode description]
     *
     * @return [description]
     */
    public String getResolutionCode() {
        return null;
    }

    /**
     * [getState description]
     *
     * @return [description]
     */
    public String getState() {
        return null;
    }

    /**
     * [getSubject description]
     *
     * @return [description]
     */
    public String getSubject() {
        return subject;
    }

    /**
     * [getTicketId description]
     *
     * @return [description]
     */
    public int getTicketId() {
        return ticketId;
    }

    /**
     * [getTicketType description]
     *
     * @return [description]
     */
    public TicketType getTicketType() {
        return null;
    }

    /**
     * [getTicketTypeString description]
     *
     * @return [description]
     */
    public String getTicketTypeString() {
        return null;
    }

    /**
     * [setCaller description]
     *
     * @param str [description]
     */
    private void setCaller(String str) {
    	//Empty method for now
    }

    /**
     * [setCancellationCode description]
     *
     * @param str [description]
     */
    private void setCancellationCode(String str) {
    	//Empty method for now
    }

    /**
     * [setCategory description]
     *
     * @param str [description]
     */
    private void setCategory(String str) {
    	//Empty method for now
    }

    /**
     * [setOwner description]
     *
     * @param str [description]
     */
    private void setOwner(String str) {
    	//Empty method for now
    }

    /**
     * [setFeedbackCode description]
     *
     * @param str [description]
     */
    private void setFeedbackCode(String str) {
    	//Empty method for now
    }

    /**
     * [setPriority description]
     *
     * @param str [description]
     */
    private void setPriority(String str) {
    	//Empty method for now
    }

    /**
     * [setResolutionCode description]
     *
     * @param str [description]
     */
    private void setResolutionCode(String str) {
    	//Empty method for now
    }

    /**
     * [setState description]
     *
     * @param str [description]
     */
    private void setState(String str) {
    	//Empty method for now
    }

    /**
     * [setSubject description]
     *
     * @param str [description]
     */
    private void setSubject(String str) {
    	//Empty method for now
    }

    /**
     * [setTicketType description]
     *
     * @param str [description]
     */
    private void setTicketType(String str) {
    	//Empty method for now
    }

    /**
     * [toString description]
     *
     * @return [description]
     */
    public String toString() {
        return null;
    }

    /**
     * [update description]
     *
     * @param command [description]
     */
    public void update(Command command) {
    	//Empty method for now
    }

    /** Enumeration */
    public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
    /** Enumeration */
    public enum Priority { URGENT, HIGH, MEDIUM, LOW }
    /** Enumeration */
    public enum TicketType { REQUEST, INCIDENT }

    /**
     * FeedbackState
     */
    public class FeedbackState implements TicketState {
        /**
         * [FeedbackState description]
         */
        private FeedbackState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }

    /**
     * ClosedState
     */
    public class ClosedState implements TicketState {
        /**
         * [ClosedState description]
         */
        private ClosedState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }

    /**
     * ResolvedState
     */
    public class ResolvedState implements TicketState {
        /**
         * [ResolvedState description]
         */
        private ResolvedState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }

    /**
     * NewState
     */
    public class NewState implements TicketState {
        /**
         * [NewState description]
         */
        private NewState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }

    /**
     * WorkingState
     */
    public class WorkingState implements TicketState {
        /**
         * [WorkingState description]
         */
        private WorkingState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }

    /**
     * CanceledState
     */
    public class CanceledState implements TicketState {
        /**
         * [CanceledState description]
         */
        private CanceledState() {

        }
		@Override
		public void updateState(Command command) {
			// TO-DO Auto-generated method stub

		}

		@Override
		public String getStateName() {
			// TO-DO Auto-generated method stub
			return null;
		}
    }
}
