package edu.ncsu.csc216.ticket_manager.model.manager;

import edu.ncsu.csc216.ticket_manager.model.command.Command;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Category;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Priority;
import edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType;

/**
 * TicketManager
 * 
 * @author Abhay
 */
public class TicketManager {
    /**
     * [TicketManager description]
     */
    private TicketManager() {

    }

    /**
     * [getInstance description]
     *
     * @return [description]
     */
    public static TicketManager getInstance() {
		return null;
    }

    /**
     * [saveTicketsToFile description]
     *
     * @param fileName [description]
     */
    public void saveTicketsToFile(String fileName) {
    	//Empty method for now
    }

    /**
     * [loadTicketsFromFile description]
     *
     * @param fileName [description]
     */
    public void loadTicketsFromFile(String fileName) {
    	//Empty method for now
    }

    /**
     * [createNewTicketList description]
     */
    public void createNewTicketList() {
    	//Empty method for now
    }

    /**
     * [getTicketsForDisplay description]
     *
     * @return [description]
     */
    public String[][] getTicketsForDisplay() {
        return null;
    }

    /**
     * [getTicketsForDisplayByType description]
     *
     * @param  type [description]
     * @return      [description]
     */
    public String[][] getTicketsForDisplayByType(TicketType type) {
        return null;
    }

    /**
     * [getTicketById description]
     *
     * @param  id [description]
     * @return    [description]
     */
    public Ticket getTicketById(int id) {
        return null;
    }

    /**
     * [executeCommand description]
     *
     * @param x       [description]
     * @param command [description]
     */
    public void executeCommand(int x, Command command) {
    	//Empty method for now
    }

    /**
     * [deleteTicketById description]
     *
     * @param id [description]
     */
    public void deleteTicketById(int id) {
    	//Empty method for now
    }

    /**
     * [addTicketToList description]
     *
     * @param type     [description]
     * @param str1     [description]
     * @param str2     [description]
     * @param category [description]
     * @param priority [description]
     * @param str3     [description]
     */
    public void addTicketToList(TicketType type, String str1, String str2, Category category, Priority priority, String str3) {
    	//Empty method for now
    }
}
