package edu.ncsu.csc216.ticket_manager.model.command;

/**
 * Command
 *
 * @author Abhay
 */
public class Command {
    /** Constant */
    public static final String F_CALLER         = "Awaiting Caller";
    /** Constant */
	public static final String F_CHANGE         = "Awaiting Change";
    /** Constant */
    public static final String F_PROVIDER       = "Awaiting Prvider";
    /** Constant */
    public static final String RC_COMPLETED     = "Completed";
    /** Constant */
    public static final String RC_NOT_COMPLETED = "Not Completed";
    /** Constant */
    public static final String RC_SOLVED        = "Solved";
    /** Constant */
    public static final String RC_WORKAROUND    = "Workaround";
    /** Constant */
    public static final String RC_NOT_SOLVED    = "Not Solved";
    /** Constant */
    public static final String RC_CALLER_CLOSED = "Caller Closed";
    /** Constant */
    public static final String CC_DUPLICATE     = "Duplicate";
    /** Constant */
    public static final String CC_INAPPROPRIATE = "Inappropriate";

    /** Constant */
    private String ownerID;
    /** Constant */
    private String note;

    /**
     * [Command description]
     *
     * @param a [description]
     * @param b [description]
     * @param c [description]
     * @param d [description]
     * @param e [description]
     * @param f [description]
     */
    public Command(CommandValue a, String b, FeedbackCode c, ResolutionCode d, CancellationCode e, String f) {
        //TO-DO
    }

    /**
     * [getCommand description]
     *
     * @return [description]
     */
    public CommandValue getCommand() {
        CommandValue x = null;
        return x;
    }

    /**
     * [getOwnerId description]
     *
     * @return [description]
     */
    public String getOwnerId() {
        return ownerID;
    }

    /**
     * [getResolutionCode description]
     *
     * @return [description]
     */
    public ResolutionCode getResolutionCode() {
        ResolutionCode x = null;
        return x;
    }

    /**
     * [getNote description]
     *
     * @return [description]
     */
    public String getNote() {
        return note;
    }

    /**
     * [getFeedbackCode description]
     *
     * @return [description]
     */
    public FeedbackCode getFeedbackCode() {
        FeedbackCode x = null;
        return x;
    }

    /**
     * [getCancellationCode description]
     *
     * @return [description]
     */
    public CancellationCode getCancellationCode() {
        CancellationCode x = null;
        return x;
    }

    /** Enumeration */
    public enum CommandValue { PROCESS, FEEDBACK, RESOLVE, CONFIRM, REOPEN, CANCEL }
    /** Enumeration */
    public enum FeedbackCode { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_PROVIDER }
    /** Enumeration */
    public enum ResolutionCode { COMPLETED, NOT_COMPLETED, SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
    /** Enumeration */
    public enum CancellationCode { DUPLICATE, INAPPROPRIATE }
}
