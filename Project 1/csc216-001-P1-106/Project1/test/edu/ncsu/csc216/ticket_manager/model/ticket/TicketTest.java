/**
 *
 */
package edu.ncsu.csc216.ticket_manager.model.ticket;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * TicketTest
 *
 * @author Abhay
 */
public class TicketTest {

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#incrementCounter()}.
	 */
	@Test
	public void testIncrementCounter() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#setCounter(int)}.
	 */
	@Test
	public void testSetCounter() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#Ticket(edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType, java.lang.String, java.lang.String, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Category, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Priority, java.lang.String)}.
	 */
	@Test
	public void testTicketTicketTypeStringStringCategoryPriorityString() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#Ticket(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.ArrayList)}.
	 */
	@Test
	public void testTicketIntStringStringStringStringStringStringStringStringArrayListOfString() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getCaller()}.
	 */
	@Test
	public void testGetCaller() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getCancellationCode()}.
	 */
	@Test
	public void testGetCancellationCode() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getCategory()}.
	 */
	@Test
	public void testGetCategory() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getFeedbackCode()}.
	 */
	@Test
	public void testGetFeedbackCode() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getNotes()}.
	 */
	@Test
	public void testGetNotes() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getOwner()}.
	 */
	@Test
	public void testGetOwner() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getPriority()}.
	 */
	@Test
	public void testGetPriority() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getResolutionCode()}.
	 */
	@Test
	public void testGetResolutionCode() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getState()}.
	 */
	@Test
	public void testGetState() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getSubject()}.
	 */
	@Test
	public void testGetSubject() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getTicketId()}.
	 */
	@Test
	public void testGetTicketId() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getTicketType()}.
	 */
	@Test
	public void testGetTicketType() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#getTicketTypeString()}.
	 */
	@Test
	public void testGetTicketTypeString() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#toString()}.
	 */
	@Test
	public void testToString() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.ticket.Ticket#update(edu.ncsu.csc216.ticket_manager.model.command.Command)}.
	 */
	@Test
	public void testUpdate() {
		// TODO
	}

}
