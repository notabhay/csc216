/**
 *
 */
package edu.ncsu.csc216.ticket_manager.model.manager;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * TicketManagerTest
 *
 * @author Abhay
 */
public class TicketManagerTest {

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#getInstance()}.
	 */
	@Test
	public void testGetInstance() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#saveTicketsToFile(java.lang.String)}.
	 */
	@Test
	public void testSaveTicketsToFile() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#loadTicketsFromFile(java.lang.String)}.
	 */
	@Test
	public void testLoadTicketsFromFile() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#createNewTicketList()}.
	 */
	@Test
	public void testCreateNewTicketList() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#getTicketsForDisplay()}.
	 */
	@Test
	public void testGetTicketsForDisplay() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#getTicketsForDisplayByType(edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType)}.
	 */
	@Test
	public void testGetTicketsForDisplayByType() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#getTicketById(int)}.
	 */
	@Test
	public void testGetTicketById() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#executeCommand(int, edu.ncsu.csc216.ticket_manager.model.command.Command)}.
	 */
	@Test
	public void testExecuteCommand() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#deleteTicketById(int)}.
	 */
	@Test
	public void testDeleteTicketById() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketManager#addTicketToList(edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType, java.lang.String, java.lang.String, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Category, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Priority, java.lang.String)}.
	 */
	@Test
	public void testAddTicketToList() {
		// TODO
	}

}
