/**
 *
 */
package edu.ncsu.csc216.ticket_manager.model.manager;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * TicketListTest
 *
 * @author Abhay
 */
public class TicketListTest {

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#TicketList()}.
	 */
	@Test
	public void testTicketList() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#addTicket(edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType, java.lang.String, java.lang.String, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Category, edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.Priority, java.lang.String)}.
	 */
	@Test
	public void testAddTicket() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#addTickets(java.util.List)}.
	 */
	@Test
	public void testAddTickets() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#getTickets()}.
	 */
	@Test
	public void testGetTickets() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#getTicketsByType(edu.ncsu.csc216.ticket_manager.model.ticket.Ticket.TicketType)}.
	 */
	@Test
	public void testGetTicketsByType() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#getTicketById(int)}.
	 */
	@Test
	public void testGetTicketById() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#executeCommand(int, edu.ncsu.csc216.ticket_manager.model.command.Command)}.
	 */
	@Test
	public void testExecuteCommand() {
		// TODO
	}

	/**
	 * Test method for {@link edu.ncsu.csc216.ticket_manager.model.manager.TicketList#deleteTicketById(int)}.
	 */
	@Test
	public void testDeleteTicketById() {
		// TODO
	}

}
