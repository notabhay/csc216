package edu.ncsu.csc216.wolf_scheduler.io;

import java.util.*;
import java.io.*;

import edu.ncsu.csc216.wolf_scheduler.course.Course;

/**
 * Reads Course records from text files.  Writes a set of CourseRecords to a file.
 *
 * @author Abhay Sriwastawa
 */
public class CourseRecordIO {

    /**
     * Reads course records from a file and generates a list of valid Courses.  Any invalid
     * Courses are ignored.  If the file to read cannot be found or the permissions are incorrect
     * a File NotFoundException is thrown.
     * @param fileName file to read Course records from
     * @return a list of valid Courses
     * @throws FileNotFoundException if the file cannot be found or read
     */
	public static ArrayList<Course> readCourseRecords(String fileName)
        throws FileNotFoundException {
	    Scanner fileReader = new Scanner(new FileInputStream(fileName));
	    ArrayList<Course> courses = new ArrayList<Course>();
	    while (fileReader.hasNextLine()) {
	        try {
	            Course course = readCourse(fileReader.nextLine());
	            boolean duplicate = false;
	            for (int i = 0; i < courses.size(); i++) {
	                Course c = courses.get(i);
	                if (course.getName().equals(c.getName()) &&
	                        course.getSection().equals(c.getSection())) {
	                    //it's a duplicate
	                    duplicate = true;
	                }
	            }
	            if (!duplicate) {
	                courses.add(course);
	            }
	        } catch (IllegalArgumentException e) {
	            //skip the line
	        }
	    }
	    fileReader.close();
	    return courses;
	}

    /**
     * Returns a Course object created by parsing the line of text. This is a private helper method of readCourseRecords().
     *
     * @param nextLine nextLine
     * @return Returns a Course object created by parsing the line of text.
     */
    private static Course readCourse(String nextLine) {
        Scanner lineReaderCheck = new Scanner(nextLine);
        lineReaderCheck.useDelimiter(",");
        int counter = 0;
        while (lineReaderCheck.hasNext()) {
            lineReaderCheck.next();
            counter++;
        }
        lineReaderCheck.close();
        String name = "";
        String title = "";
        String section = "";
        int credits = 0;
        String instructorId = "";
        String meetingDays = "";
        int startTime = 0;
        int endTime = 0;
        if (counter == 8) {
            Scanner lineReader = new Scanner(nextLine);
            lineReader.useDelimiter(",");
            try {
                name = lineReader.next();
                title = lineReader.next();
                section = lineReader.next();
                credits = lineReader.nextInt();
                instructorId = lineReader.next();
                meetingDays = lineReader.next();
                startTime = lineReader.nextInt();
                endTime = lineReader.nextInt();
                lineReader.close();
            } catch(NoSuchElementException e) {
                throw new IllegalArgumentException();
            }
        } else if (counter == 6) {
            Scanner lineReader = new Scanner(nextLine);
            lineReader.useDelimiter(",");
            try {
                name = lineReader.next();
                title = lineReader.next();
                section = lineReader.next();
                credits = lineReader.nextInt();
                instructorId = lineReader.next();
                meetingDays = lineReader.next();
                lineReader.close();
            } catch(NoSuchElementException e) {
                throw new IllegalArgumentException();
            }
        }
        Course x = new Course(name, title, section, credits, instructorId, meetingDays, startTime, endTime);
        return x;
	}
}
